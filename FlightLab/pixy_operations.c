#define EXTERN

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>


#include "pixy_example_threaded.h"

#define image_height 200
#define image_width 320
#define side 5 //length of one half of cross
#define sqrt2 1.142135
#define calibObjDist1 10
#define nCalibHeights 6

double object_distances[4][4] = {{0, side*sqrt2, 2*side, side*sqrt2},
                                 {side*sqrt2, 0, side*sqrt2, 2*side},
                                 {2*side, side*sqrt2, 0, side*sqrt2},
                                 {side*sqrt2, 2*side, side*sqrt2, 0}};
                                 
double obj2target_distances[4][4] ={{0, side/sqrt2, side, side/sqrt2},
                                    {side/sqrt2, 0, side/sqrt2, side},
                                    {side, side/sqrt2, 0, side/sqrt2},
                                    {side/sqrt2, side, side/sqrt2, 0}};
                                    
double calibHeights[nCalibHeights] = {12, 14, 14.55, 15.63, 19.23, 19.78};
double calibPixDists[nCalibHeights] = {189.17/calibObjDist1, 165.87/calibObjDist1, 155/calibObjDist1, 147.8/calibObjDist1, 124.68/calibObjDist1, 121.15/calibObjDist1};

double Zconverge = 0.1;

void targetLocationQuadrotor(state_t *currState, gsl_vector *pointInQuad)
{
        //if nObjects<2 return failure
        if(currState->nobject<2)
                return;
        
        //get 3d point location in pixy frame
        gsl_vector *pointInPixy = gsl_vector_calloc (4);
        targetLocationPixy(currState, pointInPixy);
        printf("POINT IN PIXY %lf, %lf, %lf\n", gsl_vector_get(pointInPixy, 0), gsl_vector_get(pointInPixy, 1), gsl_vector_get(pointInPixy, 2));
        
        //set up rotation matrix
        gsl_matrix *p2q_mat = gsl_matrix_calloc (4, 4);         
        gsl_matrix_set_identity (p2q_mat);
        gsl_matrix_set(p2q_mat,1, 1, cos(-M_PI));
        gsl_matrix_set(p2q_mat,1, 2, -sin(-M_PI));
        gsl_matrix_set(p2q_mat,2, 1, sin(-M_PI));
        gsl_matrix_set(p2q_mat,2, 2, cos(-M_PI));
        gsl_matrix_set(p2q_mat,1, 3, -4);
        
        //Multiply & Normalize
        gsl_blas_dgemv(CblasNoTrans, 1.0, p2q_mat, pointInPixy, 0.0, pointInQuad);        
        double normalize = gsl_vector_get (pointInQuad, 3);
        gsl_vector_set(pointInQuad, 0, gsl_vector_get (pointInQuad, 0)/normalize);
        gsl_vector_set(pointInQuad, 1, gsl_vector_get (pointInQuad, 1)/normalize);
        gsl_vector_set(pointInQuad, 2, gsl_vector_get (pointInQuad, 2)/normalize);
        gsl_vector_set(pointInQuad, 3, 1.0);
        printf("POINT IN QUAD %lf, %lf, %lf\n", gsl_vector_get(pointInQuad, 0), gsl_vector_get(pointInQuad, 1), gsl_vector_get(pointInQuad, 2));         
}

int seenCombination(int combi1, int combi2, int n, int *seenCombinations)
{
    if(n==0)
        return -1;
    
    for(int i=0; i<n; i++)
    {
        if(seenCombinations[i]==combi1 || seenCombinations[i]==combi2)
            return 1;
    }    
}

void targetLocationPixy(state_t *currState, gsl_vector *pointInPixy)
{
	if(currState->nobject<2)
                return;
        printf("\n\n\n****************************TARGET**************************\n");
        double X, Y;       
        
        /***location of target in pixels***/
        int nTimes = 1;
        double Tx, Ty;
        int *seenCombinations = (int*)malloc(sizeof(int)*100);
        
        for(int k=0; k<currState->nobject; k++)
        {
                int flag;
                for(int l=0; l<currState->nobject; l++)
                {
                        int combi1 = currState->indices[k]*10 + currState->indices[l];
                        int combi2 = currState->indices[l]*10 + currState->indices[k];
                        
                        
                        if(k==l)
                            {printf("%dsame indices\n", combi1); continue;}
                        //if((int)seenCombination(combi1, combi2, nTimes-1, seenCombinations)==1))
                           //{ printf("%d: seen combination\n", combi1); continue; }    
                                     
                       
                       seenCombinations[nTimes-1] = combi1;
                       for(int m=0; m<nTimes; m++)
                            printf("%d   ", seenCombinations[m]);
                       printf("\n");    
                       int i = currState->indices[k], j = currState->indices[l];                
                       double midX, midY, Vx, Vy, mag_V, U_Vx, U_Vy;
                        
                        //if the points aren't adjacent - opposite
                        if(abs(i-j)==2)
                        {
                        	printf("i-j > 2 \n");
                        	Vx = (*(currState->pixy_x[i]->filt) + *(currState->pixy_x[j]->filt))/2;
                        	Vy = (*(currState->pixy_y[i]->filt) + *(currState->pixy_y[j]->filt))/2;
                        	printf("Object locations: %lf %lf %lf %lf\n", *(currState->pixy_x[i]->filt), *(currState->pixy_y[i]->filt),
                        	       *(currState->pixy_x[j]->filt), *(currState->pixy_y[j]->filt));
                        	printf("Target in Pixel, %lf %lf\n", Vx, Vy);
                        }
                        //else if they are adjacent
                        else
                        {
                                //setting up a vector
                                int lowInd, highInd;
                                if((i==0 && j==3) || (i==3 && j==0))
                                {
                                       highInd = 0; 
                                       lowInd = 3; 
                                }
                                else
                                {
                                        if (i>j)
                                        { 
                                                lowInd = i; 
                                                highInd = j; 
                                        }
                                        else
                                        { 
                                                lowInd = j; 
                                                highInd = i; 
                                        }
                                }
                                        
                                //calculate mid point
                                midX = (*(currState->pixy_x[lowInd]->filt) + *(currState->pixy_x[highInd]->filt))/2;
                                midY = (*(currState->pixy_y[lowInd]->filt) + *(currState->pixy_y[highInd]->filt))/2;
                                
                                //vector from mp to one vertex
                                Vx = *(currState->pixy_x[highInd]->filt) - midX;
                                Vy = *(currState->pixy_y[highInd]->filt) - midY;
                                
                                //rotating it by 90 degrees
                                Vx =  Vx*cos(-M_PI/2) + Vy*sin(-M_PI/2);
                                Vy = -Vx*sin(-M_PI/2) + Vy*cos(-M_PI/2);
                                
                                //unit vector in the direction
                                mag_V = sqrt(pow(Vx,2) + pow(Vy,2));
                                U_Vx = Vx/mag_V;
                                U_Vy = Vy/mag_V;
                                
                                //magnitude in the direction
                                double c2tDist = obj2target_distances[i][j];
                                c2tDist = c2tDist/currState->p2wfactor;
                                
                                //target location
                                Vx = U_Vx * c2tDist;
                                Vy = U_Vy * c2tDist;                                 
                        
                                //target x, y in pixels
                                Vx = midX + Vx;
                                Vy = midY + Vy;
                        }
                        
                        //average of as many times
                        Tx = (Tx*(nTimes-1)+Vx)/nTimes; 
                        Ty = (Ty*(nTimes-1)+Vy)/nTimes;   
                        printf("Mean target location %lf %lf\n", Tx, Ty);                
                                
                        nTimes++;   
                        flag = 1; break;
                                           
                }
                if(flag==1) break;
        }
        
        /****Translating taarget to image center***/
        Tx = Tx - image_width/2;
	    Ty = Ty - image_height/2;    
	    printf("Shifted target location %lf %lf\n", Tx, Ty);
        
        /****Convert to world units***/       
        X = Tx * 1/currState->p2wfactor;
	    Y = Ty * 1/currState->p2wfactor;
	    printf("Target in pixy frame %lf %lf %lf\n", X, Y, currState->Z);
	    printf("******************************************************\n\n");
        gsl_vector_set(pointInPixy, 0, X);
        gsl_vector_set(pointInPixy, 1, Y);
        gsl_vector_set(pointInPixy, 2, currState->Z);
        gsl_vector_set(pointInPixy, 3, 1.0);
        //exit(0);
}


int dist2D(int x1, int y1, int x2, int y2)
{
        printf("%d %d %d %d\n", x1, y1, x2, y2);
        return (int)(sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)));
}

double lookupZ(double p2wfactor)
{
        double Z = 0;
        int i;
        for(i=0; i<nCalibHeights; i++)
        {
                if(p2wfactor >= calibPixDists[i])
					break;
        }
        printf("%lf, %lf\n", calibHeights[i-1], calibHeights[i]);
        float m = (calibHeights[i] - calibHeights[i-1])/(calibPixDists[i] - calibPixDists[i-1]);
        float c = calibHeights[i] - m*calibPixDists[i];

        Z = (m*p2wfactor+c);
        return Z;
}

int ifExists(int i, int n, int *indices)
{
    for(int j=0; j<n; j++)
        if(i==indices[j])
            return 1;
    
    return -1;
}


int calculateZ(state_t *currState)
{
        double Z;
        currState->Zprev = currState->Z;
        if(currState->nobject<2)
                return -1;
                
        /***getting Z height***/
        //collecting indices of seen objects
        int idx = 0;                
        for(int i=0; i<TARGET_NUM; i++)
        {
                if((int)(*(currState->pixy_x[i]->filt))!=0)
                { 
                	//printf("%lf\t", *(currState->pixy_x[i]->filt));
                	currState->indices[idx] = i;                	
                	printf("%d\n", currState->indices[idx]);
                	idx++;
            	}
                         
        }
        
        //checking if opposite targets are present
        int indFrom, indTo;
        if(ifExists(1, currState->nobject, currState->indices)==1 && ifExists(3, currState->nobject, currState->indices)==1){
    //       printf("opposite present 13\n"); 
             indFrom =1; indTo = 3;}
        
        else if(ifExists(0, currState->nobject, currState->indices)==1 && ifExists(2, currState->nobject, currState->indices)==1)
        {
            //printf("opposite present 02\n"); 
            indFrom =0; indTo = 2;
        }
        else
        {
    //         printf("No oposite present\n"); 
           indFrom =currState->indices[0]; indTo = currState->indices[1];
        }
        
        
        //look up real distance between 2 objects
        double objDist = object_distances[indFrom][indTo];
    //    printf("obj dist: %lf\n", objDist);
        
        //calculate pixel distance        
        double x1 = *(currState->pixy_x[indFrom]->filt), y1 = *(currState->pixy_y[indFrom]->filt);
        double x2 = *(currState->pixy_x[indTo]->filt), y2 = *(currState->pixy_y[indTo]->filt);        
            
        double pixDist = (double)dist2D((int)x1, (int)y1, (int)x2, (int)y2);
    //    printf("%lf, %lf, %lf, %lf, %lf \n", x1, y1, x2, y2, pixDist);
        
        //lookup & calculate Z for the pixDist to objDist ratio       
        currState->p2wfactor = pixDist/objDist;
        currState->Z = lookupZ(currState->p2wfactor);
        
   //     printf("height: %lf\n", currState->Z);
   //     printf("prev height: %lf\n",currState->Zprev);
        double diff = currState->Zprev - currState->Z;
   //     printf("Diff %lf\n", diff);
        
        if(fabs(diff) < Zconverge && currState->Z > 5 && currState->Zprev > 5)
                {printf("converged\n"); return 1;}
        else
                return -1;
}
