#ifndef __PIXY_EXAMPLE_THREADED__
#define __PIXY_EXAMPLE_THREADED__

/* Example of receiving LCM messages and processing them in another thread.

   pthread_create(...) starts a thread
   pthread_join(...) waits for a thread to finish
   pthread_mutex is used to prevent the receive thread from modifying the
    shared data when the processing thread is reading it
 */

#include <stdio.h>
#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>
#include <lcm/lcm.h>
#include <math.h>
#include "bbblib/gnc.h"
#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/imu_t.h"
#include <glib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

// Define targets
#define TARGET_NUM 4
#define TARGET_0 12
#define TARGET_1 20
#define TARGET_2 29
#define TARGET_3 11

typedef struct imu_data imu_data_t;
struct imu_data {
	double raw[NUM_MED];
	double med[NUM_MED];
	double *filt;
};

typedef struct pixy_data pixy_data_t;
struct pixy_data {
	double raw[NUM_MED];
	double med[NUM_MED];
	double *filt;
};


typedef struct state state_t;
struct state 
{
        // lcm
        lcm_t *lcm;
        // Data
        imu_data_t *gyro[3], *accel[3];
        int16_t nobject;
        pixy_data_t *pixy_x[TARGET_NUM], *pixy_y[TARGET_NUM], *pixy_width[TARGET_NUM], *pixy_height[TARGET_NUM], *pixy_angle[TARGET_NUM];
        double pos_last;

        double Z, Zprev;
        int *indices;
        double p2wfactor;
        gsl_vector *targetPos;
};

void targetLocationQuadrotor(state_t *currState, gsl_vector *pointInQuad);
void targetLocationPixy(state_t *currState, gsl_vector *pointInPixy);
int dist2D(int x1, int y1, int x2, int y2);
double lookupZ(double p2wfactor);


int calculateZ(state_t *currState);
int seenCombination(int combi1, int combi2, int n, int *seenCombinations);
#endif
