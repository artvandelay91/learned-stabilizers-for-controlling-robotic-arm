#ifndef __COMMAND__
#define __COMMAND__

#include "bbblib/bbb.h"
#include "bbblib/kinematics.h"
#include "pixy_example_threaded.h"
#define H_NEUTRAL 1
#define H_FOLLOW 2
#define STO_H -2.1
#define XLIM 3
#define YLIM 3.5

//set the servos to a particular value
int setServo(double *pos1, const double *servoZeroDeg, double *servoResolution, double *pw, double *last_pos);

//follow routine
void follow(state_t *state, double *pos, double *theta, const double *servoZeroDeg, double *servoResolution, double *pw,
            double *limit, int *goforit, double *last_pos);

//trajectory planning stuff
void trajectory(double *pos_0, double *pos_f, int num_pts,
                const double *servoZeroDeg, double *servoResolution, double *pw, double *last_pos);

#endif
