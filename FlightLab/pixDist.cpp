#include <stdio.h>
#include <math.h>

int main()
{
    double x1 =  220;
    double y1 =  138;
    double x2 =  103;
    double y2 =  145;
    
    double dist = sqrt(pow((x2-x1), 2) + pow((y2-y1), 2));
    
    printf("%lf\n", dist);
    return 0; 
}
