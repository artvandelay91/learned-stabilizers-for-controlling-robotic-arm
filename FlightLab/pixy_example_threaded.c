
/* Example of receiving LCM messages and processing them in another thread.

   pthread_create(...) starts a thread
   pthread_join(...) waits for a thread to finish
   pthread_mutex is used to prevent the receive thread from modifying the
    shared data when the processing thread is reading it
 */

#define EXTERN


#include "pixy_example_threaded.h"
#include "command.h"


pthread_mutex_t data_mutex;
int msg_count = 0;

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    printf("Received message on channel %s, timestamp %" PRId64 "\n",
           channel, msg->utime);

        state_t *state = userdata;

        int i;
    // Do some processing of the pixy data here
    // Caution: don't save the msg pointer; the data won't be valid after
    //  this handler function exists
    printf("Number of objects: %d\n", msg->nobjects);

	printf("state objects: %d\n", state->nobject);
    state->nobject = msg->nobjects;
	for (i = 0; i < TARGET_NUM; i += 1) {
		free(state->pixy_x[i]->filt);
		free(state->pixy_y[i]->filt);
		free(state->pixy_width[i]->filt);
		free(state->pixy_height[i]->filt);
		//state->pixy_angle[i]->filt = NULL;
	}
	
    for (i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];
        printf("%d\n", obj->signature);
		printf("number of object = %d: ", i+1);

		// Determine which target
        int target;
       if (obj->signature == TARGET_0) target = 0;
        else if(obj->signature == TARGET_1) target = 1;
        else if(obj->signature == TARGET_2) target = 2;
        else if(obj->signature == TARGET_3) target = 3;
        else continue;

        if (obj->type == PIXY_T_TYPE_COLOR_CODE) {
            printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   obj->x, obj->y, obj->width, obj->height, obj->angle);
			DataFilter(obj->x, state->pixy_x[target]->raw, state->pixy_x[target]->med, state->pixy_x[target]->filt); 
			printf("x filtered: %lf\n",*(state->pixy_x[target]->filt));
			DataFilter(obj->y, state->pixy_y[target]->raw, state->pixy_y[target]->med, state->pixy_y[target]->filt); 
			DataFilter(obj->width, state->pixy_width[target]->raw, state->pixy_width[target]->med,state->pixy_width[target]->filt); 
			DataFilter(obj->height, state->pixy_height[target]->raw, state->pixy_height[target]->med, state->pixy_height[target]->filt); 
			DataFilter(obj->angle, state->pixy_angle[target]->raw, state->pixy_angle[target]->med, state->pixy_angle[target]->filt); 
        } 
		else {
            printf("Signature %d at (%d, %d) size (%d, %d)\n",
                   obj->signature, obj->x, obj->y, obj->width, obj->height);
        }
    }

    if((int)calculateZ(state)==1)
        targetLocationQuadrotor(state, state->targetPos);
    
    // Lock mutex before modifying shared data
    pthread_mutex_lock(&data_mutex);

    // Dumb example of modifying shared data in a global variable
    msg_count += 1;

    pthread_mutex_unlock(&data_mutex);
}

void imu_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                  const imu_t *msg, void *userdata)
{
	state_t *state = userdata;
    printf("Received message on channel %s\n", channel);
    printf("gyro (deg/sec) = (%.3lf, %.3lf, %.3lf),\n ", msg->gyro[0],msg->gyro[1], msg->gyro[2]);
    printf("accel (g) = (%.3lf, %.3lf, %.3lf), \n", msg->accel[0], msg->accel[1], msg->accel[2]);
	DataFilter(msg->gyro[0], state->gyro[0]->raw, state->gyro[0]->med, state->gyro[0]->filt);
	DataFilter(msg->gyro[1], state->gyro[1]->raw, state->gyro[1]->med, state->gyro[1]->filt);
	DataFilter(msg->gyro[2], state->gyro[2]->raw, state->gyro[2]->med, state->gyro[2]->filt);
	DataFilter(msg->accel[0], state->accel[0]->raw, state->accel[0]->med, state->accel[0]->filt);
	DataFilter(msg->accel[1], state->accel[1]->raw, state->accel[1]->med, state->accel[1]->filt);
	DataFilter(msg->accel[2], state->accel[2]->raw, state->accel[2]->med, state->accel[2]->filt);
}

void* lcm_receive_loop(void *data)
{
	state_t *state = data;      

    pixy_frame_t_subscribe(state->lcm, "PIXY", pixy_handler, state);
	imu_t_subscribe(state->lcm, "imu", imu_handler, state);

    while (1) {
        lcm_handle(state->lcm);
    }

    lcm_destroy(state->lcm);
}

void* processing_loop(void *data)
{
	state_t *state = data;
	int hz = 1;

    while (1) {

        // Lock the mutex before reading shared data
        pthread_mutex_lock(&data_mutex);

        printf("msg_count: %d\n", msg_count);

        // We're done -- unlock the mutex
        pthread_mutex_unlock(&data_mutex);

        usleep(1000000/hz);
    }
}

state_t *
state_create (void)
{
    state_t *state = calloc(1,sizeof(*state));
    // Data
    for (int k = 0; k < TARGET_NUM; k++) {
		state->pixy_x[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_x[k]->raw[NUM_MED] = 0;
		state->pixy_x[k]->med[NUM_MED] = 0;
		state->pixy_x[k]->filt = calloc(1,sizeof(double));
		state->pixy_y[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_y[k]->raw[NUM_MED] = 0;
		state->pixy_y[k]->med[NUM_MED] = 0;
		state->pixy_y[k]->filt = calloc(1,sizeof(double));
		state->pixy_width[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_width[k]->raw[NUM_MED] = 0;
		state->pixy_width[k]->med[NUM_MED] = 0;
		state->pixy_width[k]->filt = calloc(1,sizeof(double));
		state->pixy_height[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_height[k]->raw[NUM_MED] = 0;
		state->pixy_height[k]->med[NUM_MED] = 0;
		state->pixy_height[k]->filt = calloc(1,sizeof(double));
		state->pixy_angle[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_angle[k]->raw[NUM_MED] = 0;
		state->pixy_angle[k]->med[NUM_MED] = 0;
		state->pixy_angle[k]->filt = calloc(1,sizeof(double));
    }

    for (int k = 0; k < 3; k++) {
		state->gyro[k] = calloc(1,sizeof(imu_data_t));
		state->gyro[k]->raw[NUM_MED] = 0;
		state->gyro[k]->med[NUM_MED] = 0;
		state->gyro[k]->filt = calloc(1,sizeof(double));
		state->accel[k] = calloc(1,sizeof(imu_data_t));
		state->accel[k]->raw[NUM_MED] = 0;
		state->accel[k]->med[NUM_MED] = 0;
		state->accel[k]->filt = calloc(1,sizeof(double));
	}
	state->nobject = 0;
	state->Z = 0; 
	state->Zprev = 100;
	state->indices = calloc(4, sizeof(int));
	state->targetPos = gsl_vector_calloc(4);
    
    // lcm
    state->lcm = lcm_create(NULL);

    //last commanded sero position
    state->pos_last;

    return state;
}

int main()
{
	state_t *state = state_create();    
	// Initialize the mutex
	pthread_mutex_init(&data_mutex, NULL);

	// Start the threads
	pthread_t lcm_receive_thread;
	pthread_t processing_thread;
	pthread_create(&lcm_receive_thread, NULL, lcm_receive_loop, state);
	pthread_create(&processing_thread, NULL, processing_loop, state);

	// Wait for thread to finish (in this case, thread will never finish
	// because it's in an infinite loop)
	pthread_join(lcm_receive_thread, NULL);
}
