#ifndef __REXARM__
#define __REXARM__

// core api
#include "vx/vx.h"
#include "vx/vx_util.h"
#include "vx/vx_remote_display_source.h"
#include "vx/gtk/vx_gtk_display_source.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "math/gsl_util_blas.h"
#include "math/gsl_util_vector.h"
#include "math/gsl_util_matrix.h"
#include "math/dijkstra.h"
#include "common/zarray.h"
#include <stdbool.h>

// drawables
#include "vx/vxo_drawables.h"

#ifdef __cplusplus
extern "C" {
#endif

int
rexarm_clamp (double q[4]);
void
checkErrorState(const double angles[4], int errors[4], float* arm_color[4]);
// 4th coord is phi
void
calculateCoord(const double angles[4], double coords[4]);
vx_object_t *
rexarm_vxo_arm(const int errors[4], const double angles[4], const float* arm_color[4], float alpha); // alpha sets the object's transparency
void 
calculateDHTable(const double angles[4],gsl_matrix* DH[4]);
bool
calculateAngles(const double coords[4], double angles[16]);
bool
chooseYaw(const double angles[16], const double angles_prev[4], double angles_goal[4]);
bool
interpolateCubic(const double angles_initial[4], const double angles_final[4], zarray_t *command_buffer, zarray_t *cylinders, zarray_t *cylinders_goal, zarray_t *cubicPath);
bool
isCollisionFree(const double angles_command[4], zarray_t *cylinders, zarray_t *cylinders_goal);
bool
angleThreshold(const double angles_current[4], const double angles_goal[4]);
bool
xyzThreshold(const double xyzp_current[4], const double xyz_goal[3]);
// obsType = 0 for cylinder, 1 for annulus
void
closestPoint(gsl_vector* CP, gsl_vector *P, double currCylinderPos[2],int obsType);
void
floatingClosestPoint(gsl_vector* FP, gsl_vector* CP, double currCylinderPos[2], gsl_matrix* DH[4], int i, int obsType);
void
pointInFrame(gsl_vector* P, gsl_matrix* DH);
gsl_vector*
pointToPoint(gsl_vector* P1,gsl_vector* P2);
void
calculatePotentialCost(const double angles_initial[4], zarray_t* cylinders, zarray_t* cylinders_goal, int goal_index, zarray_t* command_buffer, zarray_t* cp_coords);
void
constructRoadMap(const int nNodes, const int kClosest, zarray_t* cylinders, zarray_t* cylinders_goal, dijkstra_graph_t* roadMap, const double angles_initial[4], zarray_t* command_buffer, zarray_t* prmPoints, zarray_t *cubicPath);

void
planPRM(const int nNodes, const int kClosest, zarray_t* cylinders, zarray_t* cylinders_goal, dijkstra_graph_t* roadMap, const double q_initial[4], zarray_t* command_buffer, zarray_t *cubicPath);

int
zdistcmp (const void *a_pp, const void *b_pp);

#ifdef __cplusplus
}
#endif

#endif // __REXARM__
