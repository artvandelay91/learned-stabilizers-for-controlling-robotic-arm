#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>
#include <math.h>

// LCM
#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"

// core api
#include "vx/vx.h"
#include "vx/vx_util.h"
#include "vx/vx_remote_display_source.h"
#include "vx/gtk/vx_gtk_display_source.h"

// drawables
#include "vx/vxo_drawables.h"

// common
#include "common/getopt.h"
#include "common/pg.h"
#include "common/zarray.h"
#include "common/timestamp.h"


// imagesource
#include "imagesource/image_u32.h"
#include "imagesource/image_source.h"
#include "imagesource/image_convert.h"

#include "rob550_util.h"    // This is where a lot of the internals live
#include "math/math_util.h"
#include "rexarm.h"
#include "affine_calibration.h"
#include "template_matching.h"

#define NUM_SERVOS 4

bool template_annulus = 0;
bool extract_annulus = 0;
bool ann_temp_created = 0;
bool template_cylinder = 0;
bool extract_cylinder = 0;
bool cyl_temp_created = 0;
int cSgn = 0;
int aSgn = 0;

// It's good form for every application to keep its state in a struct.
typedef struct state state_t;
struct state {
    bool running;
    bool calibrating;
    bool making_cylinders;
    bool making_cylinders_goal;
    bool collision;
    bool generating_map;
	dijkstra_graph_t* roadMap;
    getopt_t        *gopt;
	
	double home[4];
    double joint_angle[4];
    double joint_angle_lcm[4];
    zarray_t *command_buffer;
    unsigned long command_index;
    double speed;
    double max_torque;
    double x_coords[4];
    double y_coords[4];
    struct affine_pair affine_pairs[4];
    zarray_t *pairs;
    zarray_t *cylinders;
    zarray_t *cylinders_goal;
    zarray_t *prmPoints;
    zarray_t *cp_coords;
    zarray_t *cubicPath;
    affine_calibration_t *cal;
    int point_counter;

    int current_coord;
	double scale;
	double camera_rotate;
	int idle_mode;
	zarray_t *saved_points;
	//gsl_vector* curSavedPoint;
	double curSavedPoint[4];
    
    double temp_ann_start[4];
    double temp_ann_stop[4];
    double temp_cyl_start[4];
    double temp_cyl_stop[4];
    	
    // LCM
    lcm_t *lcm;
    const char *command_channel;
    const char *status_channel;

    parameter_gui_t *pg;

    // image stuff
    char *img_url;
    int   img_height;
    int   img_width;
    image_u32_t frame;
    image_u32_t *template_annulus;
    image_u32_t *template_annulus_decimate;
    image_u32_t *template_cylinder;
    image_u32_t *template_cylinder_decimate;

    // vx stuff
    vx_application_t    vxapp;
    vx_world_t         *vxworld;      // where vx objects are live
    vx_event_handler_t *vxeh; // for getting mouse, key, and touch events
    vx_mouse_event_t    last_mouse_event;

    // threads
    pthread_t animate_thread;
	pthread_t status_thread;
    pthread_t command_thread;
	pthread_t map_thread;
	
    // for accessing the arrays
    pthread_mutex_t mutex;
};


static void
status_handler (const lcm_recv_buf_t *rbuf,
                const char *channel,
                const dynamixel_status_list_t *msg,
                void *user)
{
state_t *state = user;
dynamixel_status_t stat;
	for (int id = 0; id < msg->len; id++) {
		    stat = msg->statuses[id];
		    state->joint_angle_lcm[id] = stat.position_radians*57.295;
	}
	if (state->idle_mode == 1) {
		//Print out servo positions
		for (int id = 0; id < msg->len; id++) {
		    stat = msg->statuses[id];
		    //printf ("[id %d]=%6.3f ",id, stat.position_radians);
		}
		//printf ("\n");
		calculatePotentialCost(state->joint_angle_lcm, state->cylinders,state->cylinders_goal,0,NULL,state->cp_coords);
    }

}

void *
status_loop (void *data)
{
    state_t *state = data;
    dynamixel_status_list_t_subscribe (state->lcm,
                                       state->status_channel,
                                       status_handler,
                                       state);
    const int hz = 15;
    while (1) {
        // Set up the LCM file descriptor for waiting. This lets us monitor it
        // until something is "ready" to happen. In this case, we are ready to
        // receive a message.
        int status = lcm_handle_timeout (state->lcm, 1000/hz);
        if (status <= 0)
            continue;

        // LCM has events ready to be processed
    }

    return NULL;
}

void *
map_loop (void *user)
{
    state_t *state = user;
    while(1){
		//printf("LOOOOP\n");
		if(state->generating_map) {
			state ->roadMap = dijkstra_create (500+(zarray_size(state->cylinders_goal))+1, 0);
			printf("Generating PRM");
			// 500, 20 normally
			constructRoadMap(500, 40, state->cylinders, state->cylinders_goal, state->roadMap, state->joint_angle_lcm, state->command_buffer,state->prmPoints, state->cubicPath);
			state->generating_map = false;
		}
	}
    return NULL;
}

void *
command_loop (void *user)
{
    state_t *state = user;
    state->collision = false;
    const int hz = 30;
    dynamixel_command_list_t cmds;
    cmds.len = NUM_SERVOS;
    cmds.commands = calloc (NUM_SERVOS, sizeof(dynamixel_command_t));
	int errors[4]; float* arm_color[4];
    while (1) {
        // Send LCM commands to arm. Normally, you would update positions, etc,
        // but here, we will just home the arm.
        //printf("angle 1 preclamp: %f\n",state->joint_angle[0]);
        
        // If there are unsent commands...
        //printf("commands in buffer: %d\tindex: %d\n",zarray_size(state->command_buffer), state->command_index);
        if((zarray_size(state->command_buffer) > 1) && (state->idle_mode == 0)){
        	zarray_get (state->command_buffer, 0, state->joint_angle);
        	
        	if(state->command_index == 0){
        		zarray_remove_index (state->command_buffer, 0, false);
        		state->command_index = 1;
       		}
        	else if(angleThreshold(state->joint_angle_lcm,state->joint_angle)){
        		zarray_remove_index (state->command_buffer, 0, false);
        		//printf("cbuf size:%d\n",zarray_size(state->command_buffer));
       		}
        	
        }
		    
		    	
    	zarray_get (state->command_buffer, 0, state->joint_angle);
    	//printf("sending command\n");
		rexarm_clamp(state->joint_angle);
		checkErrorState(state->joint_angle, errors, arm_color);
		if(errors[0]) state->collision=true;
		else state->collision=false;
		//printf("angle 1 clamped: %f\n",state->joint_angle[0]);
		if(state->idle_mode == 0){
			for (int id = 0; id < NUM_SERVOS; id++) {
				if(true)
				{
		
					cmds.commands[id].utime = utime_now ();

					cmds.commands[id].position_radians = (state->joint_angle[id])/57.295;
					cmds.commands[id].speed = state->speed;
					cmds.commands[id].max_torque = state->max_torque;
				}
			}
		} else {
			for (int id = 0; id < NUM_SERVOS; id++) {
				cmds.commands[id].utime = utime_now ();
				cmds.commands[id].position_radians = 0.0;
				cmds.commands[id].speed = 0.0;
				cmds.commands[id].max_torque = 0.0;
			}
		}
		dynamixel_command_list_t_publish (state->lcm, state->command_channel, &cmds);
		usleep (1000000/hz);
    }

    free (cmds.commands);

    return NULL;
}

// === Parameter listener =================================================
// This function is handed to the parameter gui (via a parameter listener)
// and handles events coming from the parameter gui. The parameter listener
// also holds a void* pointer to "impl", which can point to a struct holding
// state, etc if need be.
static void
my_param_changed (parameter_listener_t *pl, parameter_gui_t *pg, const char *name)
{
	state_t *currState = pl->impl;
	int i;
	zarray_t *command_buffer_buffer;
	command_buffer_buffer = zarray_create (sizeof(double[4]));
	
    if (0==strcmp ("sa1", name) || 0==strcmp ("sa2", name) || 0==strcmp ("sa3", name) || 0==strcmp ("sa4", name))
    {
        //printf ("%s = %f\n", name, pg_gd (pg, name));
        zarray_get (currState->command_buffer, 0, currState->joint_angle);
        currState->joint_angle[atoi(&name[2])-1] = pg_gd (pg, name);
        zarray_clear(currState->command_buffer);
        //zarray_add (currState->command_buffer, currState->home);
        currState->command_index=1;
        zarray_add (currState->command_buffer, currState->joint_angle);
        
        // Test Potential Field Planning
        //calculatePotentialCost(currState->joint_angle_lcm, currState->cylinders,currState->cylinders_goal,0,currState->command_buffer);
        //printf ("%s = %f\tstate angle: %f\n", name, pg_gd (pg, name),currState->joint_angle[atoi(&name[2])-1]);
        
    }
    else if (0==strcmp ("sdb", name))
    {
        printf ("%s = %f\n", name, pg_gd_boxes (pg, name));
        currState->speed = pg_gd_boxes (pg, name);
    }
    else if (0==strcmp ("tdb", name))
    {
        printf ("%s = %f\n", name, pg_gd_boxes (pg, name));
        currState->max_torque = pg_gd_boxes (pg, name);
    }
    else if (0==strcmp ("but_home", name))
    {
        printf("Returning Home\n");
        zarray_clear(currState->command_buffer);
        zarray_add (currState->command_buffer, currState->home);
        //zarray_add (currState->command_buffer, currState->home);
        currState->command_index=1;
    }
    else if (0==strcmp ("but_calibrate", name))
    {
        printf("Launching Calibration\n");
        currState->calibrating = 1;
        currState->current_coord = 0;
    }
    else if (0==strcmp ("but_reset_scale", name))
    {
        
        printf("Reset Scale to .2\n");
        currState->scale = 0.2;
    }
    else if (0==strcmp ("but_add_cylinder", name))
    {
        currState->making_cylinders = 1 - currState->making_cylinders;
        currState->making_cylinders_goal = 0;
        printf("Making cylinders set to %d\n",currState->making_cylinders);
    }
    else if (0==strcmp ("but_add_cylinder_goal", name))
    {
        currState->making_cylinders_goal = 1 - currState->making_cylinders_goal;
        currState->making_cylinders = 0;
        printf("Making Goals set to %d\n",currState->making_cylinders_goal);
    }
    else if (0==strcmp ("but_set_coord", name))
    {
    	double coords[4] = {152,-166,126,0};
    	
        calculateAngles(coords,currState->joint_angle);
        zarray_add (currState->command_buffer, currState->joint_angle);
        printf("Joint Angles: %f, %f, %f, %f\n",currState->joint_angle[0],currState->joint_angle[1],currState->joint_angle[2],currState->joint_angle[3]);
    }
    else if (0==strcmp ("but_set_idle", name))
    {
        currState->idle_mode = 1 - currState->idle_mode;
        if(currState->idle_mode == 0){
        	zarray_clear(currState->command_buffer);
        	zarray_add (currState->command_buffer, currState->home);
   	    	//zarray_add (currState->command_buffer, currState->home);
   		    currState->command_index=1;
        }
        printf("Idle Mode set to %d\n",currState->idle_mode);
    }
    else if (0==strcmp ("but_rec_pos", name))
    {
        calculateCoord(currState->joint_angle_lcm, currState->curSavedPoint);
        zarray_add (currState->saved_points, currState->curSavedPoint);
        //printf("Joint Angles: %f, %f, %f, %f\n",currState->joint_angle_lcm[0],currState->joint_angle_lcm[1],currState->joint_angle_lcm[2],currState->joint_angle_lcm[3]);
        printf("Point # %d, [%f,%f,%f], phi %f\n",zarray_size(currState->saved_points),currState->curSavedPoint[0],currState->curSavedPoint[1],currState->curSavedPoint[2],currState->curSavedPoint[3]);
    }
    else if (0==strcmp ("but_playback", name))
    {
        double coords[4], joint_angle_opt[16],joint_angle_choice[4], starting_config[4];
        bool continuous = 0;
        int i;
        for(i=0;i<4;i++) starting_config[i] = currState->joint_angle_lcm[i];
        if(0==strcmp ("but_playback_cont", name)) continuous = 1;
        do {
			zarray_get (currState->saved_points, currState->point_counter, coords);
			
			calculateAngles(coords, joint_angle_opt);
			chooseYaw(joint_angle_opt,starting_config, joint_angle_choice);
			
			if(interpolateCubic(starting_config, joint_angle_choice,command_buffer_buffer, NULL,NULL, NULL)) zarray_add_all(currState->command_buffer, command_buffer_buffer);
			else printf("Collision on path, no can do\n");
			printf("Goal Point #%d [%f,%f,%f], phi %f\n",currState->point_counter+1,coords[0],coords[1],coords[2],coords[3]);
			currState->point_counter++;
			for(i=0;i<4;i++) starting_config[i] = joint_angle_choice[i];
			if(currState->point_counter >= zarray_size(currState->saved_points))
			{
				printf("Resetting Playback\n");
				currState->point_counter = 0;
				break;
			}
        } while(continuous);
        
    }
    else if (0==strcmp ("but_playback_cont", name))
    {
        double coords[4], joint_angle_opt[16],joint_angle_choice[4], starting_config[4];
        bool continuous = 0;
        int i;
        for(i=0;i<4;i++) starting_config[i] = currState->joint_angle_lcm[i];
        if(0==strcmp ("but_playback_cont", name)) continuous = 1;
        do {
        	
			zarray_get (currState->saved_points, currState->point_counter, coords);
			
			calculateAngles(coords, joint_angle_opt);
			chooseYaw(joint_angle_opt,starting_config, joint_angle_choice);
			/*
			if(interpolateCubic(starting_config, joint_angle_choice,command_buffer_buffer, NULL,NULL, NULL)) zarray_add_all(currState->command_buffer, command_buffer_buffer);
			else printf("Collision on path, no can do\n");
			*/
			interpolateCubic(starting_config, joint_angle_choice,command_buffer_buffer, NULL,NULL, NULL);
			
			printf("Goal Point #%d [%f,%f,%f], phi %f\n",currState->point_counter+1,coords[0],coords[1],coords[2],coords[3]);
			currState->point_counter++;
			for(i=0;i<4;i++) starting_config[i] = joint_angle_choice[i];
			if(currState->point_counter >= zarray_size(currState->saved_points))
			{
				zarray_add_all(currState->command_buffer, command_buffer_buffer);
				zarray_clear(command_buffer_buffer);
				printf("Resetting Playback & adding to buffer\n");
				currState->point_counter = 0;
				break;
			}
        } while(continuous);
        
    }
	else if (0==strcmp ("but_go_goal_0", name))
    {
    	printf("Calling potential field planner to goal 0\n");
        calculatePotentialCost(currState->joint_angle_lcm, currState->cylinders,currState->cylinders_goal,0,currState->command_buffer,currState->cp_coords);
    }
    else if (0==strcmp ("but_prm_map", name))
    {
    	printf("preflag\n");
    	currState->generating_map = true;
    	printf("postflag\n");
    }
    else if (0==strcmp ("but_prm_goal", name))
    {
    	planPRM(500, 40, currState->cylinders, currState->cylinders_goal, currState->roadMap, currState->joint_angle_lcm, currState->command_buffer, currState->cubicPath);
    }
    
    else if(0==strcmp ("but_locate_cylinder", name))
    {
        int *ptr = NULL;
        if(cyl_temp_created)
         {
                double decimate = 3.0;
	        image_u32_t *decimatedTemplate = image_util_u32_decimate(currState->template_cylinder, decimate);
	        image_u32_t *decimatedFrame = image_util_u32_decimate (&currState->frame, decimate);
	        
	        zarray_t *cylinderLocations =  zarray_create (sizeof(int[2]));
	        printf("Locating all cylinders\n");
	        getMatchLocations(decimatedFrame,decimatedTemplate,ptr, cylinderLocations);
                printf("# Cylinders: %d\n", zarray_size(cylinderLocations));
                
                for(int i=0;i<zarray_size(cylinderLocations);i++)
                {
                        int pos[2];
                        zarray_get(cylinderLocations, i, pos);
                        int x = 1296 - (pos[0] * decimate);
                        int y = 964 - (pos[1] * decimate);

                        double width  = currState->frame.width;
                        double height = currState->frame.height;

                        double scale = 2.0/width;

                        double gx    = ((x - width/2)*currState->scale);
	                double gy    = -(y - height - height/2)*currState->scale;
                        gy = cSgn*gy;

                        double p_pos[2];
                        double w_pos[2];
                        p_pos[0] = gx;
                        p_pos[1] = gy;

                        affine_calibration_p2w(currState->cal, p_pos, w_pos);
                        w_pos[1] = w_pos[1]-964;
                        
                        zarray_add (currState->cylinders, w_pos);
                }	
        }  
    }

    else if(0==strcmp ("but_locate_annulus", name))
    {
      int *ptr = NULL;
      if(ann_temp_created)
      {
	double decimate = 3.0;
	image_u32_t *decimatedTemplate = image_util_u32_decimate (currState->template_annulus, decimate);
	image_u32_t *decimatedFrame = image_util_u32_decimate (&currState->frame, decimate);
	
	zarray_t *annulusLocations =  zarray_create (sizeof(int[2]));
	printf("Locating all annulus\n");
	getMatchLocations(decimatedFrame,decimatedTemplate,ptr, annulusLocations);
	printf("# Annulus: %d\n", zarray_size(annulusLocations));       

	for(int i=0;i<zarray_size(annulusLocations);i++)
	{
	  int pos[2];
          zarray_get(annulusLocations, i, pos);
          int x = 1296 - (pos[0] * decimate);
          int y = 964 - (pos[1] * decimate);

	  double width  = currState->frame.width;
	  double height = currState->frame.height;

	  double gx    = ((x - width/2)*currState->scale);
	  double gy    = -(y - height - height/2)*currState->scale;
	  gy = aSgn*gy;

	  double p_pos[2];
	  double w_pos[2];	  
	  p_pos[0] = gx;
	  p_pos[1] = gy;
	  
	  affine_calibration_p2w(currState->cal, p_pos,w_pos);
          w_pos[1] = w_pos[1] - 964;
         
          zarray_add (currState->cylinders_goal, w_pos);
	}
      }
    }
    else if (0==strcmp ("cb1", name) || 0==strcmp ("cb2", name))
        printf ("%s = %d\n", name, pg_gb (pg, name));
    else
        printf ("%s changed\n", name);
}

static int
mouse_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    state_t *state = vxeh->impl;

    // vx_camera_pos_t contains camera location, field of view, etc
    // vx_mouse_event_t contains scroll, x/y, and button click events
    
    static unsigned char first_vertex = 1;    
    if(template_annulus)
    {
	if ((mouse->button_mask & VX_BUTTON1_MASK))
	{	    
	    vx_ray3_t ray;
	    vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);	    
	    double ground[3];
	    vx_ray3_intersect_xy (&ray, 0, ground);
	    	 
	    if(first_vertex)
	    {
		state->temp_ann_start[0] = ground[0];
		state->temp_ann_start[1] = ground[1];
		state->temp_ann_start[2] = mouse->x;
		state->temp_ann_start[3] = mouse->y;
		
		first_vertex = 0;
	    }	    
	    state->temp_ann_stop[0] = ground[0];
	    state->temp_ann_stop[1] = ground[1];
	    state->temp_ann_stop[2] = mouse->x;
	    state->temp_ann_stop[3] = mouse->y;		
	}
    }
    else if(template_cylinder)
    {
	if ((mouse->button_mask & VX_BUTTON1_MASK))
	{	    
	    vx_ray3_t ray;
	    vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);   
	    double ground[3];
	    vx_ray3_intersect_xy (&ray, 0, ground);	    
	 
	    if(first_vertex)
	    {
		state->temp_cyl_start[0] = ground[0];
		state->temp_cyl_start[1] = ground[1];
		state->temp_cyl_start[2] = mouse->x;
		state->temp_cyl_start[3] = mouse->y;
		
		first_vertex = 0;
	    }
	    state->temp_cyl_stop[0] = ground[0];
	    state->temp_cyl_stop[1] = ground[1];
	    state->temp_cyl_stop[2] = mouse->x;
	    state->temp_cyl_stop[3] = mouse->y;	
	}
    }
    else
	first_vertex = 1;
    
    
    if ((mouse->button_mask & VX_BUTTON1_MASK) &&
        !(state->last_mouse_event.button_mask & VX_BUTTON1_MASK)) {

        vx_ray3_t ray;
        vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);

        double ground[3];
        double world[2];
        vx_ray3_intersect_xy (&ray, 0, ground);
        printf ("Mouse clicked at coords: [%8.3f, %8.3f]  ", mouse->x, mouse->y);
        if(state->calibrating){
        	printf("Storing coord %d\n",(state->current_coord)+1);
        	state->x_coords[state->current_coord] = ground[0];
        	state->y_coords[state->current_coord] = ground[1];
        	state->affine_pairs[state->current_coord].pixel[0] = ground[0];
        	state->affine_pairs[state->current_coord].pixel[1] = ground[1];    	
        	zarray_add (state->pairs, &(state->affine_pairs[state->current_coord]));
	
        	state->current_coord++;
        	if(state->current_coord > 3){
        		state->scale = (sqrt(pow(state->x_coords[1]-state->x_coords[0],2) + pow(state->y_coords[1]-state->y_coords[0],2)))/605.0;
        		printf("scale: %f\n",state->scale);
        		//affine_calibration_print(state->pairs);
        		state->cal = affine_calibration_create (state->pairs);
        		state -> calibrating = 0;
        		
        	}
        	printf("Ground clicked at coords: [%6.3f, %6.3f]\n", ground[0], ground[1]);
        } else {
        	double pixels[2] = {ground[0], ground[1]};
        	affine_calibration_p2w(state->cal, pixels, world);
        	printf("Ground clicked at coords: [%6.3f, %6.3f]\n", world[0], world[1]);
        }
        
        if(state->making_cylinders){
        	zarray_add (state->cylinders, world);
        	printf("Cylinder Added: [%f,%f]\n",world[0],world[1]);
        }
		if(state->making_cylinders_goal){
        	zarray_add (state->cylinders_goal, world);
        	printf("Goal Added: [%f,%f]\n",world[0],world[1]);
        }
        
        
    }

    // store previous mouse event to see if the user *just* clicked or released
    state->last_mouse_event = *mouse;

    return 0;
}

static int
key_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_key_event_t *key)
{
    if(!key->released && key->key_code == 0x0061){
	extract_annulus = 0;
	if(!template_annulus)
	    template_annulus = 1;
    }
    else if(key->released && key->key_code == 0x0061){
	template_annulus = 0;	
	//Extract template
	extract_annulus = 1;
	ann_temp_created = 0;
    }
    else if(!key->released && key->key_code == 0x0063){
	extract_cylinder = 0;
	if(!template_cylinder)
	    template_cylinder = 1;
    }
    else if(key->released && key->key_code == 0x0063){
	template_cylinder = 0;	
	extract_cylinder = 1;
	cyl_temp_created = 0;
    }


    return 0;
}

static int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0; // Does nothing
}

// === Your code goes here ================================================
// The render loop handles your visualization updates. It is the function run
// by the animate_thread. It periodically renders the contents on the
// vx world contained by state
void *
animate_thread (void *data)
{
    const int fps = 60;
    state_t *state = data;
    int i;
	
	
    // Set up the imagesource
    image_source_t *isrc = image_source_open (state->img_url);

    if (isrc == NULL)
        printf ("Error opening device.\n");
    else {
        // Print out possible formats. If no format was specified in the
        // url, then format 0 is picked by default.
        // e.g. of setting the format parameter to format 2:
        //
        // --url=dc1394://bd91098db0as9?fidx=2
        for (int i = 0; i < isrc->num_formats (isrc); i++) {
            image_source_format_t ifmt;
            isrc->get_format (isrc, i, &ifmt);
            printf ("%3d: %4d x %4d (%s)\n",
                    i, ifmt.width, ifmt.height, ifmt.format);
        }
        isrc->start (isrc);
    }

    // Continue running until we are signaled otherwise. This happens
    // when the window is closed/Ctrl+C is received.
    state->collision = false;
    double render_angle[4];
    while (state->running) {
		zarray_get (state->command_buffer, 0, render_angle);
        // Get the most recent camera frame and render it to screen.
        if (isrc != NULL) {
            image_source_data_t *frmd = calloc (1, sizeof(*frmd));
            int res = isrc->get_frame (isrc, frmd);
            if (res < 0)
                printf ("get_frame fail: %d\n", res);
            else {
                // Handle frame
                image_u32_t *im = image_convert_u32 (frmd);
                state->frame = *im;
                if (im != NULL) {
                    vx_object_t *vim = vxo_image_from_u32(im,
                                                          VXO_IMAGE_FLIPY,
                                                          VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);

                    // render the image centered at the origin and at a normalized scale of +/-1 unit in x-dir
					//double scale = 2./im->width;
					/*
					vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image"),
                                        vxo_chain (vxo_mat_rotate_z(state->camera_rotate),
                                        			//vxo_mat_scale3 (state->scale, state->scale, 1.0),
                                                   vxo_mat_translate3 (-im->width/2., -im->height/2., 0.),
                                                   vim));
                    */
 /*
					vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image"),
                                        vxo_chain (vxo_mat_scale3 (state->scale, state->scale, 1.0),
                                                   vxo_mat_translate3 (-im->width/2., -im->height/2., 0.),
                                                   vim));
					
*/


					double T[16]={1,0,0,0, 
								  0,1,0,0,
								  0,0,1,0,
								  0,0,0,1};
					int j;
					if(!state->calibrating)
					{
						for(j=0;j<9;j++)
						{
							//printf("index %d\n",j+(int)floor(j/3));
							T[j+(int)floor(j/3)] = gsl_matrix_get(state->cal->calMatrix,floor(j/3),j%3);
						}
						T[2] = 0;
						T[6] = 0;
						for(j=0;j<16;j++)
						{
							//printf("T value %f\n",T[j]);
							
						}
					}
				
                    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image"),
                                        vxo_chain (
                                        			vxo_mat_translate3 (-im->width/2., -im->height/2., 0.),
                                        			vxo_mat_rotate_z(state->camera_rotate),
                                        	//		vxo_mat_copy_from_doubles(T),
                                       				//vxo_mat_scale3 (state->scale, state->scale, 1.0),			
                                       				vim));                               
	                                                  
                    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image"));
                    
                    image_u32_t *rotImg = image_u32_copy(im);
                            
                    for(int i=0; i<im->height; i++)
                    {
                        for(int j=0; j<im->width; j++)
                        {
                                int p = im->width -1 - j;
                                //int q = im->height - j;
                                //printf("%d, %d\t%d,%d\n", p, i, j, i);
                                rotImg->buf[i*im->stride+p] = im->buf[i*im->stride+j];
                                
                        }
                        //exit(0);
                    }
                    
                    image_u32_t *rotImg1 = image_u32_copy(rotImg);
                    for(int i=0; i<im->height; i++)
                    {
                        for(int j=0; j<im->width; j++)
                        {
                                int p = im->height -1 - i;
                                //int q = im->height - j;
                                //printf("%d, %d\t%d,%d\n", p, i, j, i);
                                rotImg1->buf[p*rotImg->stride+j] = rotImg->buf[i*im->stride+j];
                                
                        }
                        //exit(0);
                    }
	             
	             
	             
	             double t_width_cyl = 0;

		    if(extract_cylinder)
		    {			
			double t_width_g  = fabs(state->temp_cyl_start[0] - state->temp_cyl_stop[0]);
			double t_height_g = fabs(state->temp_cyl_start[1] - state->temp_cyl_stop[1]);
			
			double t_width_p  = (t_width_g/state->scale);
			double t_height_p = (t_height_g/state->scale);

			double x = (state->temp_cyl_start[0]/state->scale + im->width/2);
			double y = fabs(im->height - (state->temp_cyl_start[1]/state->scale + im->height/2));
			
			cSgn = (im->height - (state->temp_cyl_start[1]/state->scale + im->height/2))/fabs((im->height - (state->temp_cyl_start[1]/state->scale + im->height/2)));
				
			if(!cyl_temp_created)
			{    
                                
			    state->template_cylinder     =  image_u32_create((int)t_width_p,(int)t_height_p);

			    
			    printf("template created\n");
			    cyl_temp_created = 1;
			
			    for(int i=0;i<(int)t_height_p;i++)
			    {
				for(int j=0;j<(int)t_width_p;j++)
				{	
				    state->template_cylinder->buf[i*state->template_cylinder->stride + ((int)t_width_p - 1 - j)] = rotImg1->buf[((int)(y)+j)*im->stride + ((int)(x)-i)]; 
				    //printf("%d %d template extracting\n",(int)(y)+i,(int)(x)+j);
				}
			    } 
			}
			vx_object_t *vim2 = vxo_image_from_u32(state->template_cylinder,VXO_IMAGE_FLIPY,
							       VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
			
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image3"),vxo_pix_coords(VX_ORIGIN_BOTTOM_LEFT,vxo_chain(vxo_mat_translate3(0,0,0),vim2)));
			
			vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image3"));
		    }



                    //GRABBING ANNULUS**************************************************************************		    
		    if(extract_annulus)
		    {
			double t_width_g  = fabs(state->temp_ann_start[0] - state->temp_ann_stop[0]);
			double t_height_g = fabs(state->temp_ann_start[1] - state->temp_ann_stop[1]);
			
			double t_width_p  = (t_width_g/state->scale);
			double t_height_p = (t_height_g/state->scale);
			
			double x = (state->temp_ann_start[0]/state->scale + im->width/2);
			double y = fabs(im->height - (state->temp_ann_start[1]/state->scale + im->height/2));
			aSgn = (im->height - (state->temp_ann_start[1]/state->scale + im->height/2))/fabs((im->height - (state->temp_ann_start[1]/state->scale + im->height/2)));

			if(!ann_temp_created)
			{
			  
			  state->template_annulus = image_u32_create((int)t_width_p,(int)t_height_p);

			  printf("template created\n");
			  ann_temp_created = 1;
			  
			  for(int i=0;i<(int)t_height_p;i++)
			  {
			    for(int j=0;j<(int)t_width_p;j++)
			    {	
			      state->template_annulus->buf[i*state->template_annulus->stride + ((int)t_width_p - 1 - j)] = rotImg1->buf[((int)(y)+j)*im->stride + ((int)(x)-i)]; 
			      //printf("%d %d template extracting\n",(int)(y)+i,(int)(x)+j);
			    }
			  }	  
			}
			
			vx_object_t *vim2 = vxo_image_from_u32(state->template_annulus,VXO_IMAGE_FLIPY,
							       VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
			
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image2"),vxo_pix_coords(VX_ORIGIN_BOTTOM_LEFT,vxo_chain(vxo_mat_translate3(t_width_cyl,0,0),vim2)));
			
			vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image2"));
			
			
		    }  
                    image_u32_destroy (im);
                    image_u32_destroy (rotImg);
                    image_u32_destroy (rotImg1);
                }
            }
            fflush (stdout);
            isrc->release_frame (isrc, frmd);
        }

        // Example rendering of vx primitives
        //double rad = (vx_util_mtime () % 5000) * 2. * M_PI / 5e3;   // [ 0, 2PI]
        //double osc = ((vx_util_mtime () % 5000) / 5e3) * 2. - 1;    // [-1, 1]

        // Creates a blue box and applies a series of rigid body transformations
        // to it. A vxo_chain applies its arguments sequentially. In this case,
        // then, we rotate our coordinate frame by rad radians, as determined
        // by the current time above. Then, the origin of our coordinate frame
        // is translated 0 meters along its X-axis and 0.5 meters along its
        // Y-axis. Finally, a 0.1 x 0.1 x 0.1 cube (or box) is rendered centered at the
        // origin, and is rendered with the blue mesh style, meaning it has
        // solid, blue sides.
        /*vx_object_t *vxo_sphere = vxo_chain (vxo_mat_rotate_z (rad),
                                             vxo_mat_translate2 (0, 0.5),
                                             vxo_mat_scale (0.1),
                                             vxo_sphere (vxo_mesh_style (vx_blue))); 

        // Then, we add this object to a buffer awaiting a render order
        vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "rot-sphere"), vxo_sphere);*/

        // Now we will render a red box that translates back and forth. This
        // time, there is no rotation of our coordinate frame, so the box will
        // just slide back and forth along the X axis. This box is rendered
        // with a red line style, meaning it will appear as a red wireframe,
        // in this case, with lines 2 px wide at a scale of 0.1 x 0.1 x 0.1.
        /*
        vx_object_t *vxo_square = vxo_chain (vxo_mat_scale (state->scale),
        									 vxo_mat_translate2 (0, 0),
                                             vxo_mat_scale3(30, 100, 50),
                                             vxo_box (vxo_mesh_style (vx_red)));
        */
                                             

        double cylinder_pixels[2];
        
        vx_object_t *vxo_cylinder;
        vx_object_t *vxo_cylinder_goal;
        vx_object_t *vxo_prm_coords;
        vx_object_t *vxo_cp_coords;
        vx_object_t *vxo_cubicPath;
        for(i=0;i<zarray_size(state->cylinders);i++)
		{
			zarray_get (state->cylinders, i, cylinder_pixels);
			vxo_cylinder = vxo_chain (vxo_mat_scale (state->scale),
        							  vxo_mat_translate3 (cylinder_pixels[0], cylinder_pixels[1],90),
                                      vxo_mat_scale3(92, 92, 145),
                                      vxo_cylinder (vxo_mesh_style (vx_red)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "osc-cylinder"), vxo_cylinder);
			//printf("pixel %d: (%f,%f)\n",i,cylinder_pixels[0],cylinder_pixels[1]);
		}
		double cylinder_goal_pixels[2];
		for(i=0;i<zarray_size(state->cylinders_goal);i++)
		{
			zarray_get (state->cylinders_goal, i, cylinder_goal_pixels);
			vxo_cylinder_goal = vxo_chain (vxo_mat_scale (state->scale),
        							  vxo_mat_translate3 (cylinder_goal_pixels[0], cylinder_goal_pixels[1],10),
                                      vxo_mat_scale3(75, 75, 20),
                                      vxo_cylinder (vxo_mesh_style (vx_blue)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "osc-cylinder"), vxo_cylinder_goal);
			//printf("pixel %d: (%f,%f)\n",i,cylinder_pixels[0],cylinder_pixels[1]);
		}

        // We add this object to a different buffer so it may be rendered
        // separately if desired
        //vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "osc-square"), vxo_square);
        
        double nodeCoords[4];
        for(i=0;i<zarray_size(state->prmPoints);i++)
		{
			zarray_get (state->prmPoints, i, nodeCoords);
			vxo_prm_coords = vxo_chain (vxo_mat_scale (state->scale),
        							  vxo_mat_translate3 (nodeCoords[0], nodeCoords[1], nodeCoords[2]),
                                      vxo_mat_scale(4),
                                      vxo_sphere (vxo_mesh_style (vx_yellow)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "points"), vxo_prm_coords);
		}

		double cpCoords[4];
        for(i=0;i<zarray_size(state->cp_coords);i++)
		{
			zarray_get (state->cp_coords, i, cpCoords);
			vxo_cp_coords = vxo_chain (vxo_mat_scale (state->scale),
        							  vxo_mat_translate3 (cpCoords[0], cpCoords[1], cpCoords[2]),
                                      vxo_mat_scale(10),
                                      vxo_sphere (vxo_mesh_style (vx_green)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "points"), vxo_cp_coords);
		}
		double cubicCoords[3];
        for(i=0;i<zarray_size(state->cubicPath);i++)
		{
			zarray_get (state->cubicPath, i, cubicCoords);
			vxo_cubicPath = vxo_chain (vxo_mat_scale (state->scale),
        							  vxo_mat_translate3 (cubicCoords[0], cubicCoords[1], cubicCoords[2]),
                                      vxo_mat_scale(6),
                                      vxo_sphere (vxo_mesh_style (vx_green)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "points"), vxo_cubicPath);
		}
        // Draw a default set of coordinate axes
        vx_object_t *vxo_axe = vxo_chain (vxo_mat_scale (state->scale),
        								  vxo_mat_scale (200), // 30 mm axes
                                          vxo_axes ()
                                          );
        vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "axes"), vxo_axe);
        int errors[4];
        float alpha = 5;
		float* arm_color[4];
		checkErrorState(render_angle,errors,arm_color);
		for(i=0;i<4;i++)
		{
			if(errors[i] == 1) 
			{
			        arm_color[i] = vx_red;
			        state->collision = true;
			        //break;
			}
			else 
			{
			        arm_color[i] = vx_blue;
			        state->collision = false;
			}
			
		
		}
		if (state->idle_mode == 1) 
		{
			render_angle[0] = state->joint_angle_lcm[0];
			render_angle[1] = state->joint_angle_lcm[1];
			render_angle[2] = state->joint_angle_lcm[2];
			render_angle[3] = state->joint_angle_lcm[3];
		}
        if(!state->calibrating)
		{
			vx_object_t *vxo_arm = vxo_chain(vxo_mat_scale (state->scale),
									rexarm_vxo_arm(errors,render_angle,arm_color,alpha));
		
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "arm"), vxo_arm);
		}
		
        // Now, we update both buffers
        vx_buffer_swap (vx_world_get_buffer (state->vxworld, "osc-cylinder"));
        //vx_buffer_swap (vx_world_get_buffer (state->vxworld, "rot-sphere"));
        //vx_buffer_swap (vx_world_get_buffer (state->vxworld, "osc-square"));
        vx_buffer_swap (vx_world_get_buffer (state->vxworld, "axes"));
        vx_buffer_swap (vx_world_get_buffer (state->vxworld, "arm"));
		vx_buffer_swap (vx_world_get_buffer (state->vxworld, "points"));
	
	if(template_annulus)
        {
	    float points[24] = {state->temp_ann_start[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_stop[0],state->temp_ann_stop[1],0.01,
				state->temp_ann_start[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_start[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_start[0],state->temp_ann_start[1],0.01};
	    int npoints = 8;
	    //vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    //vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "ann_template"),
		//	       vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_red, 2.0f))); 
	    
	    
	    //vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_template"));
	}
	else if(template_cylinder)
	{
	    float points[24] = {state->temp_cyl_start[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_stop[0],state->temp_cyl_stop[1],0.01,
				state->temp_cyl_start[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_start[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_start[0],state->temp_cyl_start[1],0.01};
	    int npoints = 8;
	    //vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    //vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "ann_cylinder"),
			       //vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_green, 2.0f))); 
	    
	    
	    //vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_cylinder"));
	}
	else
	{
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_template"));
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_cylinder"));
	}	
        usleep (1000000/fps);
    }

    if (isrc != NULL)
        isrc->stop (isrc);

    return NULL;
}

state_t *
state_create (void)
{
    state_t *state = calloc (1, sizeof(*state));
	
    state->vxworld = vx_world_create ();
    state->vxeh = calloc (1, sizeof(*state->vxeh));
    state->vxeh->key_event = key_event;
    state->vxeh->mouse_event = mouse_event;
    state->vxeh->touch_event = touch_event;
    state->vxeh->dispatch_order = 100;
    state->vxeh->impl = state; // this gets passed to events, so store useful struct here!

    state->vxapp.display_started = rob550_default_display_started;
    state->vxapp.display_finished = rob550_default_display_finished;
    state->vxapp.impl = rob550_default_implementation_create (state->vxworld, state->vxeh);

    state->running = 1;

    return state;
}

void
state_destroy (state_t *state)
{
    if (!state)
        return;

    free (state->vxeh);
    getopt_destroy (state->gopt);
    pg_destroy (state->pg);
    free (state);
}

// This is intended to give you a starting point to work with for any program
// requiring a GUI. This handles all of the GTK and vx setup, allowing you to
// fill in the functionality with your own code.
int
main (int argc, char *argv[])
{
    rob550_init (argc, argv);
    state_t *state = state_create ();
    state->scale = 1; //0.164307;
    state->making_cylinders = 0;
    // Deprecated, use command buffer instead
    state->joint_angle[0] = 0;
    state->joint_angle[1] = 0;
    state->joint_angle[2] = 0;
    state->joint_angle[3] = 0;
  	
  	state->home[0] = 0.0;
    state->home[1] = 90.0;
    state->home[2] = 0.0;
    state->home[3] = 0.0;
    
  	state->point_counter = 0;
    state->speed = 0.05;
    state->max_torque = 0.35;
    state->calibrating = 1;
    state->generating_map = false;
    size_t pair_sz = sizeof(struct affine_pair);
    state->pairs = zarray_create (pair_sz);
    //state->camera_rotate = 3.1415;
    
    state->command_buffer = zarray_create (sizeof(double[4]));
    state->cp_coords = zarray_create (sizeof(double[3]));
    state->cubicPath = zarray_create (sizeof(double[3]));
    zarray_add (state->command_buffer, state->home);
    zarray_add (state->command_buffer, state->home);
    state->command_index = 0;
    state->prmPoints = zarray_create (sizeof(double[4]));
    state->cylinders = zarray_create (sizeof(double[2]));
    state->cylinders_goal = zarray_create (sizeof(double[2]));
    printf("size in main: %d\n",zarray_size(state->pairs));
    
    state->affine_pairs[0].world[0] = -311.5; state->affine_pairs[0].world[1] = -305;
    state->affine_pairs[1].world[0] = 299; state->affine_pairs[1].world[1] = -305;
    state->affine_pairs[2].world[0] = 299; state->affine_pairs[2].world[1] = 299;
    state->affine_pairs[3].world[0] = -311.5; state->affine_pairs[3].world[1] = 299;
	
	state->saved_points = zarray_create (sizeof(double[4]));

    // Parse arguments from the command line, showing the help
    // screen if required
    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt,  'h', "help", 0, "Show help");
    getopt_add_bool (state->gopt, 'i', "idle", 0, "Command all servos to idle");
    getopt_add_string (state->gopt, '\0', "url", "", "Camera URL");
	getopt_add_string (state->gopt, '\0', "status-channel", "ARM_STATUS", "LCM status channel");
    getopt_add_string (state->gopt, '\0', "command-channel", "ARM_COMMAND", "LCM command channel");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }
	
    state->lcm = lcm_create (NULL);
    state->command_channel = getopt_get_string (state->gopt, "command-channel");
    state->status_channel = getopt_get_string (state->gopt, "status-channel");

    // Set up the imagesource. This looks for a camera url specified on
    // the command line and, if none is found, enumerates a list of all
    // cameras imagesource can find and picks the first url it finds.
    if (strncmp (getopt_get_string (state->gopt, "url"), "", 1)) {
        state->img_url = strdup (getopt_get_string (state->gopt, "url"));
        printf ("URL: %s\n", state->img_url);
    }
    else {
        // No URL specified. Show all available and then use the first
        zarray_t *urls = image_source_enumerate ();
        printf ("Cameras:\n");
        for (int i = 0; i < zarray_size (urls); i++) {
            char *url;
            zarray_get (urls, i, &url);
            printf ("  %3d: %s\n", i, url);
        }
        if (0==zarray_size (urls)) {
            printf ("Found no cameras.\n");
            return -1;
        }

        zarray_get (urls, 0, &state->img_url);
    }

    // Initialize this application as a remote display source. This allows
    // you to use remote displays to render your visualization. Also starts up
    // the animation thread, in which a render loop is run to update your display.
    vx_remote_display_source_t *cxn = vx_remote_display_source_create (&state->vxapp);

    // Initialize a parameter gui
    state->pg = pg_create ();
    pg_add_double_slider (state->pg, "sa1", "Servo 1", -180, 180, 0);
    pg_add_double_slider (state->pg, "sa2", "Servo 2", -120, 120, 90);
    pg_add_double_slider (state->pg, "sa3", "Servo 3", -120, 120, 0);
    pg_add_double_slider (state->pg, "sa4", "Servo 4", -120, 120, 0);
    pg_add_double_boxes (state->pg, "sdb", "Speed", .05,
    				    "tdb", "Max Torque", 0.35,
    				    NULL);
    pg_add_check_boxes (state->pg,
                        "cb1", "Check Box 1", 0,
                        "cb2", "Check Box 2", 1,
                        NULL);
    pg_add_buttons (state->pg,
                    "but_home", "Return Home",
                    "but_calibrate", "Calibrate Axes",
                    "but_reset_scale", "Reset Scale",
                    "but_add_cylinder", "Add Cylinder",
                    "but_add_cylinder_goal", "Add Goal",
                    NULL);
    pg_add_buttons (state->pg,
                    "but_set_coord", "Go to Coordinates",
                    "but_set_idle", "Toggle Idle Mode",
                    "but_rec_pos", "Record Position",
                    "but_playback", "Playback",
                    "but_playback_cont", "Continuous Playback",
                    "but_go_goal_0", "Go to Goal 0",
                    NULL);     
    pg_add_buttons (state->pg,
                    "but_prm_map", "Construct Road Map",
                    "but_prm_goal", "PRM Go to Goal",
                    NULL);             
    pg_add_buttons (state->pg, 
                   "but_locate_cylinder", "Locate Cylinders",
                   "but_locate_annulus", "Locate Annuli",
                    NULL);  
    parameter_listener_t *my_listener = calloc (1, sizeof(*my_listener));
    my_listener->impl = state;
    my_listener->param_changed = my_param_changed;
    pg_add_listener (state->pg, my_listener);
    

    // Launch our worker threads
    pthread_create (&state->animate_thread, NULL, animate_thread, state);
	
    pthread_create (&state->status_thread, NULL, status_loop, state);
    pthread_create (&state->command_thread, NULL, command_loop, state);
    pthread_create (&state->map_thread, NULL, map_loop, state);
    
    // This is the main loop
    rob550_gui_run (&state->vxapp, state->pg, 1024, 768);
    // Probably not needed, given how this operates
    pthread_join (state->status_thread, NULL);
    pthread_join (state->command_thread, NULL);
    pthread_join (state->map_thread, NULL);

    // Quit when GTK closes
    state->running = 0;
    pthread_join (state->animate_thread, NULL);

    // Cleanup
    zarray_destroy (state->pairs);
    affine_calibration_destroy (state->cal);
	lcm_destroy (state->lcm);
    free (my_listener);
    state_destroy (state);
    vx_remote_display_source_destroy (cxn);
    vx_global_destroy ();
}
