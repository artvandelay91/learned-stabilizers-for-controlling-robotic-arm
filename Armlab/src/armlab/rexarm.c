#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <math.h>
#include <unistd.h>

#include "rexarm.h"
#include "affine_calibration.h"

int
rexarm_clamp (double q[4])
{
    double max_angle[4] = {180,120,120,120}; // Degrees
    double min_angle[4] = {-180,-120,-120,-120};
    
    int i, retVal = 0;
    for(i=0;i<4;i++){
    	if(q[i] > max_angle[i]) 
    	{
    		q[i] = max_angle[i];
    		retVal = 1;
    	}
    	if(q[i] < min_angle[i]) 
    	{
    		q[i] = min_angle[i];
    		retVal = 1;
    	}
    }
    return retVal;
}

void
checkErrorState(const double angles[4], int errors[4], float* arm_color[4])
{
	gsl_matrix* DH[4];
	calculateDHTable(angles,DH);
	gsl_vector* P0 = gsl_vector_alloc (4);
	gsl_vector* x = gsl_vector_calloc (4);
	gsl_vector_set (x, 3, 1.0);
	gslu_blas_mv (P0, DH[3], x);
	
	double normalize = gsl_vector_get (P0, 3);
	gsl_vector_set (P0, 0, gsl_vector_get (P0, 0)/normalize);
	gsl_vector_set (P0, 1, gsl_vector_get (P0, 1)/normalize);
	gsl_vector_set (P0, 2, gsl_vector_get (P0, 2)/normalize);
	gsl_vector_set (P0, 3, 1);
	//printf("end effector[%f,%f,%f]\n",gsl_vector_get (P0, 0),gsl_vector_get (P0, 1),gsl_vector_get (P0, 2));
	
	if(gsl_vector_get (P0, 2) < 5)/* || 
	   (gsl_vector_get (P0, 0) >= -45 && gsl_vector_get (P0, 0) <= 45
	   && gsl_vector_get (P0, 1) >= -45 && gsl_vector_get (P0, 1) <= 45
	   && gsl_vector_get (P0, 0) <= 318))*/
	{
		//printf("z pos: %f\n",gsl_vector_get (P0, 2));
		errors [0] = 1; errors [1] = 1; errors [2] = 1; errors [3] = 1;
		//arm_color[0] = vx_red; arm_color[1] = vx_red; arm_color[2] = vx_red; arm_color[3] = vx_red;
	}
	else 
	{
		errors [0] = 0; errors [1] = 0; errors [2] = 0; errors [3] = 0;
	}
	gsl_vector_free(x);
	gsl_vector_free(P0);
	gsl_matrix_free(DH[0]);
	gsl_matrix_free(DH[1]);
	gsl_matrix_free(DH[2]);
	gsl_matrix_free(DH[3]);
}

void
calculateCoord(const double angles[4], double coords[4])
{
	gsl_matrix* DH[4];
	calculateDHTable(angles,DH);
	gsl_vector* P0 = gsl_vector_alloc (4);
	gsl_vector* x = gsl_vector_calloc (4);
	gsl_vector_set (x, 3, 1.0);
	//printf("cC: b->size=%d, A->size1 =%d, A->size2%d, x->size=%d",(int)P0->size,(int)(DH[3]->size1),(int)(DH[3]->size2),(int)x->size);
	gslu_blas_mv (P0, DH[3], x);
	
	double normalize = gsl_vector_get (P0, 3);
	gsl_vector_set (P0, 0, gsl_vector_get (P0, 0)/normalize);
	gsl_vector_set (P0, 1, gsl_vector_get (P0, 1)/normalize);
	gsl_vector_set (P0, 2, gsl_vector_get (P0, 2)/normalize);
	gsl_vector_set (P0, 3, 1);
	
	coords[0] = gsl_vector_get (P0, 0);
	coords[1] = gsl_vector_get (P0, 1);
	coords[2] = gsl_vector_get (P0, 2);
	coords[3] = abs(angles[1] + angles[2] + angles[3]) - 90.0;
	
	gsl_matrix_free(DH[0]);
	gsl_matrix_free(DH[1]);
	gsl_matrix_free(DH[2]);
	gsl_matrix_free(DH[3]);
	gsl_vector_free(P0);
	gsl_vector_free(x);
	//printf("end effector[%f,%f,%f]\n",gsl_vector_get (P0, 0),gsl_vector_get (P0, 1),gsl_vector_get (P0, 2));

	
}

vx_object_t *
rexarm_vxo_arm(const int errors[4], const double angles[4], const float* arm_color[4], float alpha)
{
	gsl_matrix* DH[4];
	calculateDHTable(angles,DH);
	double T[16];
	int i,j;
	int dia = 30, len = 52,a[4] = {30,102,101,110},wy[4] = {118, 30, 30, 5}, wz[4] = {52,52,52,5};
	double offset[4] = {0.5,0,0,0};
	vx_object_t *vxo_link;
	vx_object_t *vxo_arm = vxo_chain_create ();
	for(i=0;i<4;i++)
	{
		for(j=0;j<16;j++)
		{
			T[j] = gsl_matrix_get(DH[i],floor(j/4),j%4);
		}
		if(i<3){
			vxo_link = vxo_chain (
    					vxo_mat_copy_from_doubles(T),
    				vxo_chain (vxo_mat_scale3 (dia, dia, len),
               			vxo_cylinder (vxo_mesh_style (arm_color[i]))),
    				vxo_chain (vxo_mat_scale3 (a[i], wy[i], wz[i]),
               			vxo_mat_translate3 (-0.5+offset[i], 0.0+offset[i], 0.0),
               			vxo_box (vxo_mesh_style (vx_blue)))
    	);
		} else {
			vxo_link = vxo_chain (
    					vxo_mat_copy_from_doubles(T),
    				vxo_chain (vxo_mat_scale3 (a[i], wy[i], wz[i]),
               			vxo_mat_translate3 (-0.5+offset[i], 0.0+offset[i], 0.0),
               			vxo_box (vxo_mesh_style (vx_blue)))
    	);
		}
		vxo_chain_add (vxo_arm, vxo_link);	
	}
	
	// return your rexarm Vx model
	gsl_matrix_free(DH[0]);
	gsl_matrix_free(DH[1]);
	gsl_matrix_free(DH[2]);
	gsl_matrix_free(DH[3]);
	return vxo_arm;	
}

void
calculateDHTable(const double angles[4],gsl_matrix* DH[4])
{
	
	int d[4] = {118,0,0,0}, a[4] = {0,102,101,110}, alphas[4] = {-90,0,0,0}, angles_offset[4] = {0, -90, 0, 0};
	int i;
	for(i=0;i<4;i++)
	{
		DH[i] = gsl_matrix_alloc (4, 4);
		gsl_matrix_set_identity (DH[i]);
		double theta = (angles[i] + angles_offset[i])/57.295;
		double alpha = alphas[i]/57.295;
		gsl_matrix_set (DH[i], 0, 0, cos(theta));
		gsl_matrix_set (DH[i], 0, 1, -sin(theta)*cos(alpha));
		gsl_matrix_set (DH[i], 0, 2, sin(theta)*sin(alpha));
		gsl_matrix_set (DH[i], 0, 3, a[i]*cos(theta));
		
		gsl_matrix_set (DH[i], 1, 0, sin(theta));
		gsl_matrix_set (DH[i], 1, 1, cos(theta)*cos(alpha));
		gsl_matrix_set (DH[i], 1, 2, -cos(theta)*sin(alpha));
		gsl_matrix_set (DH[i], 1, 3, a[i]*sin(theta));
		
		gsl_matrix_set (DH[i], 2, 1, sin(alpha));
		gsl_matrix_set (DH[i], 2, 2, cos(alpha));
		gsl_matrix_set (DH[i], 2, 3, d[i]);
		
		//char fileName[100];
		//sprintf(fileName,"DH_%d.txt", i);
		//saveMatrix(DH[i], fileName);
	}
	
	gsl_matrix* dump = gsl_matrix_alloc (4, 4);
	gslu_blas_mm (dump, DH[0], DH[1]); //DH01
	gsl_matrix_memcpy (DH[1], dump);
	gslu_blas_mm (dump, DH[1], DH[2]); //DH02
	gsl_matrix_memcpy (DH[2], dump);
	gslu_blas_mm (dump, DH[2], DH[3]); //DH03
	gsl_matrix_memcpy (DH[3], dump);
	gsl_matrix_free(dump);
}

bool
calculateAngles(const double coords[4], double angles[16])
{
	bool retVal = true;
	double xg = coords[0], yg = coords[1], zg = coords[2];
	int L[4] = {118,102,101,110},i;
	double phi = coords[3]/57.295, rg = sqrt(pow(xg,2)+pow(yg,2));
	double zg_p = zg + L[3]*sin(phi), rg_p = rg - L[3]*cos(phi);
	double delta_z = zg_p - L[0], delta_r = rg_p;
	double beta = atan2(delta_z,delta_r);
	double exp1 = pow(L[2],2) - (pow(delta_z,2) + pow(delta_r,2)) - pow(L[1],2);
	double exp2 = -2*sqrt(pow(delta_z,2) + pow(delta_r,2))*L[1];
	if(fabs(exp1/exp2) > 1) retVal = false;
	double psi = acos(exp1/exp2);
	
	
	angles[0] = atan2(yg,xg);
	angles[8] = atan2(-yg,-xg);
	angles[4] = atan2(yg,xg);
	angles[12] = atan2(-yg,-xg);
	
	//printf("x=%f,y=%f,atan2=%f\n",xg,yg,angles[0]);
	double exp3 = (pow(delta_z,2) + pow(delta_r,2) - pow(L[1],2) - pow(L[2],2))/(2*L[1]*L[2]);
	if(fabs(exp3) > 1) retVal = false;
	angles[2] = acos(exp3);
	angles[6] = -angles[2];
	angles[1] = 3.1415/2 - beta - psi;
	angles[5] = 3.1415/2 - beta + psi;
	angles[3] = phi - angles[1] - angles[2] + 3.1415/2;
	angles[7] = phi - angles[5] - angles[6] + 3.1415/2;
	
	for(i=9;i<16;i++){
		if(i!=12) angles[i] = -angles[i-8];
	}
	printf("Coords from function: [%f, %f, %f]\n",coords[0],coords[1],coords[2]);
	//printf("Joint Angles from calculation: %f, %f, %f, %f\n",angles[0],angles[1],angles[2],angles[3]);
	printf("Joint Angles from calculation:");
	for(i=0;i<16;i++){
		angles[i] = angles[i]*57.295;
		if(i%4 == 0) printf("\n");
		printf("%7.3f\t",angles[i]);
	}
	printf("\n");
	
	
	// angles[1] = angles[1]*57.295; angles[2] = angles[2]*57.295; angles[3] = angles[3]*57.295;
	
	return retVal;
}


bool
chooseYaw(const double angles[16], const double angles_prev[4], double angles_goal[4])
{
	double delta_angles[4] = {0,0,0,0}, angle_set[4], delta_angle,min_delta;
	int i,j,min_index;
	bool retVal = true;
	for(i=0;i<4;i++)
	{
		angle_set[0] = angles[4*i +0];
		angle_set[1] = angles[4*i +1];
		angle_set[2] = angles[4*i +2];
		angle_set[3] = angles[4*i +3];
		
		if(rexarm_clamp(angle_set) == 0){
			for(j=0;j<4;j++){
				delta_angle = abs(angle_set[j]-angles_prev[j]);
				if(delta_angle > delta_angles[i]) delta_angles[i] = delta_angle;
			}
		} else delta_angles[i] = 200;
	}
	
	min_delta = delta_angles[0];
	
	min_index = 0;
	
	for(i=1;i<4;i++)
	{
		if(delta_angles[i] < min_delta) {
			min_index = i;
			min_delta = delta_angles[i];
		}
	}
	
	if(min_delta == 200) retVal = false;
	for(i=0;i<4;i++)
	{
		angles_goal[i] = angles[4*min_index + i];
	}
	return retVal;

}

bool
interpolateCubic(const double angles_initial[4], const double angles_final[4], zarray_t *command_buffer, zarray_t *cylinders, zarray_t *cylinders_goal, zarray_t *cubicPath)
{
	double a0[4],a2[4],a3[4],angles_command[4],delta_t=1.0, j, num_points, xyzp[4], path_coords[3];
	int i;
	bool retVal = true, collision_free = true;
	if(command_buffer == NULL) num_points = 10;
	else num_points = 30;
	
	for(i=0;i<4;i++){
		//printf("init%d\t%f, final%d\t%f\n",i,angles_initial[i],i,angles_final[i]);
		a0[i] =   angles_initial[i];
		//a1[i] =   0.0; always zero
		a2[i] =   3.0*(angles_final[i] - angles_initial[i]);
		a3[i] = - 2.0*(angles_final[i] - angles_initial[i]);
	}
	
	for(j=0;j<=delta_t;j+=(delta_t/num_points)){
		
		for(i=0;i<4;i++){
			angles_command[i] = (a0[i] + a2[i]*pow(j,2.0) + a3[i]*pow(j,3.0));
		}
		// Check for collisions
		if(cylinders != NULL && cylinders_goal != NULL) 
			collision_free = isCollisionFree(angles_command, cylinders, cylinders_goal);
		else	collision_free = true;
		if(collision_free){
			if(command_buffer != NULL) 
				zarray_add (command_buffer, angles_command);
				if(cubicPath != NULL)
				{
					calculateCoord(angles_command, xyzp);
					path_coords[0] = xyzp[0];
					path_coords[1] = xyzp[1];
					path_coords[2] = xyzp[2];
					zarray_add (cubicPath, path_coords);
				}
		} else {
			//printf("Edge invalid");
			retVal = false;
			j=delta_t; i=4;
		}
	}
	return retVal;
}

bool isCollisionFree(const double angles_command[4], zarray_t *cylinders, zarray_t *cylinders_goal){
	int i,j;
	bool retVal = true;
	gsl_vector* x = gsl_vector_calloc (4);	
	gsl_vector_set (x, 3, 1.0);
	gsl_vector* CP = gsl_vector_alloc(4);
	gsl_vector* P = gsl_vector_alloc(4);
	gsl_vector* FP = gsl_vector_alloc(4);
	gsl_matrix* DH_c[4];
	double normalize, shortest_distance, dist2Ground, dist2Cyl;
	double currCylinderPos[2],currGoalPos[2], xyzp[4];
	calculateDHTable(angles_command,DH_c);
	for(i=1;i<4;i++){
		//printf("iCF: b->size=%d, A->size1 =%d, A->size2%d, x->size=%d",(int)P->size,(int)DH_c[i]->size1,(int)DH_c[i]->size2,(int)x->size);
		gslu_blas_mv (P, DH_c[i], x);	
		normalize = gsl_vector_get (P, 3);
		gsl_vector_set (P, 0, gsl_vector_get (P, 0)/normalize);
		gsl_vector_set (P, 1, gsl_vector_get (P, 1)/normalize);
		gsl_vector_set (P, 2, gsl_vector_get (P, 2)/normalize);
		gsl_vector_set (P, 3, 1);
		for(j=0; j<(zarray_size(cylinders) + zarray_size(cylinders_goal)); j++){
				
			
			if(j<(zarray_size(cylinders)))
			{
				zarray_get (cylinders, j, currCylinderPos); 
				//closestPoint(CP, P, currCylinderPos,0);
				floatingClosestPoint(FP, CP, currCylinderPos, DH_c, i, 0);
				dist2Cyl = sqrt(pow(gsl_vector_get(P, 0)-currCylinderPos[0],2)+pow(gsl_vector_get(P, 1)-currCylinderPos[1],2));
				if((dist2Cyl < 50 && gsl_vector_get(P, 2) < 150) || gsl_vector_get(P, 2) < 0) {
					retVal = false;
					i=4; j=(zarray_size(cylinders) + zarray_size(cylinders_goal));
				}
				//printf("\nCP Cyl[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (CP, 0),gsl_vector_get (CP, 1),gsl_vector_get (CP, 2));
			}
			else
			{
				zarray_get (cylinders_goal, j-(zarray_size(cylinders)), currGoalPos); 
				//closestPoint(CP, P, currGoalPos,1);
				floatingClosestPoint(FP, CP, currGoalPos, DH_c, i, 1);
				//printf("\nCP Annulus[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (CP, 0),gsl_vector_get (CP, 1),gsl_vector_get (CP, 2));
			}
	
			//shortest_distance = sqrt(pow(gsl_vector_get (CP, 0) - gsl_vector_get (P, 0),2) 
			//		                  + pow((gsl_vector_get (CP, 1) - gsl_vector_get (P, 1)),2)
			//		                  + pow((gsl_vector_get (CP, 2) - gsl_vector_get (P, 2)),2));
			shortest_distance = sqrt(pow(gsl_vector_get (CP, 0) - gsl_vector_get (FP, 0),2) 
					                  + pow((gsl_vector_get (CP, 1) - gsl_vector_get (FP, 1)),2)
					                  + pow((gsl_vector_get (CP, 2) - gsl_vector_get (FP, 2)),2));
			
			if(shortest_distance < 5)
			{
				//printf("Collision detected!\n");
				retVal = false;
				i=4; j=(zarray_size(cylinders) + zarray_size(cylinders_goal));
			}
		}
		dist2Ground = gsl_vector_get(P, 2);
		if(dist2Ground < 20 || dist2Ground > (375 - 20)) retVal = false;
		
	}
	calculateCoord(angles_command, xyzp);
	dist2Cyl = sqrt(pow(xyzp[0]-currCylinderPos[0],2)+pow(xyzp[1]-currCylinderPos[1],2));
	if((dist2Cyl < 50 && xyzp[2] < 150) || xyzp[2] < 20) retVal = false;

	gsl_vector_free(x);
	gsl_vector_free(CP);
	gsl_vector_free(P);
	gsl_vector_free(FP);
	gsl_matrix_free(DH_c[0]);
	gsl_matrix_free(DH_c[1]);
	gsl_matrix_free(DH_c[2]);
	gsl_matrix_free(DH_c[3]);
	return retVal;
}
bool angleThreshold(const double angles_current[4], const double angles_goal[4]){
	
	const double degree_threshold = 4;
	bool retVal = true;
	int i;
	for(i=0;i<4;i++){
		if(abs(angles_current[i]-angles_goal[i]) > degree_threshold) retVal = false;
	}
	/*
	if(retVal)printf("At the point!\n");
	else printf("Not at the point yet\n"); */
	return retVal;
}

bool xyzThreshold(const double xyzp_current[4], const double xyz_goal[3]){
	
	const double xyz_threshold = 3; // mm
	bool retVal = true;
	int i;
	for(i=0;i<3;i++){
		if(abs(xyzp_current[i]-xyz_goal[i]) > xyz_threshold) retVal = false;
	}
	/*
	if(retVal)printf("At the point!\n");
	else printf("Not at the point yet\n"); */
	return retVal;
}

void
closestPoint(gsl_vector* P, gsl_vector* CP, double currCylinderPos[2], int obsType)
{
    //setup
    double cylinderHeight;
	double radius;
	double innerRadius;
	double safety_factor = 45;
	if(obsType == 0)
	{
		cylinderHeight = 145 + safety_factor;
		innerRadius = 0;
		radius = 100/2 + safety_factor;
	}
	else 
	{
		cylinderHeight = 20;
		innerRadius = 30/2;
		radius = 68/2;
	}
	double dist2Circle = sqrt(pow((gsl_vector_get(P, 0) - currCylinderPos[0]),2) + pow((gsl_vector_get(P, 1) - currCylinderPos[1]),2));
	if(dist2Circle < radius-10 && dist2Circle > innerRadius)
	{
			gsl_vector_set(CP, 0, gsl_vector_get(P, 0));
			gsl_vector_set(CP, 1, gsl_vector_get(P, 1));
			gsl_vector_set(CP, 2, cylinderHeight);
			//printf("INSIDE CYCLINDER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");					
	}
	else
	{
		//calculation
		if(gsl_vector_get(P,2)>=cylinderHeight)
				gsl_vector_set(CP, 2, (int)cylinderHeight);
		else
				gsl_vector_set(CP, 2, (int)gsl_vector_get(P, 2));

		/*if(pow((gsl_vector_get(P,0)-currCylinderPos[0]), 2) + pow((gsl_vector_get(P,1)-currCylinderPos[1]), 2) < pow(radius,2))
		{
				gsl_vector_set(CP, 0, gsl_vector_get(P,0));
				gsl_vector_set(CP, 1, gsl_vector_get(P,1));
				//printf("INSIDE CYLINDER!!!\n");
		}*/
		//else
		//{
				double x1 = gsl_vector_get(P,0), y1 = gsl_vector_get(P,1);
				double x2 = currCylinderPos[0], y2 = currCylinderPos[1];
				
				double m = (y2-y1)/(x2-x1);
				double c = (((x2-x1)*y1) - ((y2-y1)*x1))/(x2-x1);
				
				double p = currCylinderPos[0], q = currCylinderPos[1];
				
				double A = pow(m,2) + 1;
				double B = 2*(m*c-m*q-p);
				double C = pow(q,2) - pow(radius,2) + pow(p,2) - 2*c*q + pow(c,2);
				
				double del = sqrt(pow(B,2)-4*A*C);
				double x_1 = (-B+sqrt(del))/(2*A), y_1 = m*x_1+c;
				double x_2 = (-B-sqrt(del))/(2*A), y_2 = m*x_1+c;
				
				double dist1 = sqrt(pow((x1-x_1),2) + pow((y1-y_1),2));
				double dist2 = sqrt(pow((x1-x_2),2) + pow((y1-y_2),2));

				if(dist1<=dist2)
				{
				        gsl_vector_set(CP, 0, (int)x_1);
				        gsl_vector_set(CP, 1, (int)y_1);
				       
				}
				else
				{
				        gsl_vector_set(CP, 0, (int)x_2);
				        gsl_vector_set(CP, 1, (int)y_2);
				        
				}
		//}
	}	
}

void
pointInFrame(gsl_vector* P, gsl_matrix* DH)
{

	gsl_vector* x = gsl_vector_calloc(4);
	gsl_vector_set(x,3,1);
    gslu_blas_mv (P, DH, x);	
    double normalize = gsl_vector_get (P, 3);
    gsl_vector_set (P, 0, gsl_vector_get (P, 0)/normalize);
    gsl_vector_set (P, 1, gsl_vector_get (P, 1)/normalize);
    gsl_vector_set (P, 2, gsl_vector_get (P, 2)/normalize);
	gsl_vector_set (P, 3, gsl_vector_get (P, 3)/normalize);
    gsl_vector_free(x);
}

gsl_vector*
pointToPoint(gsl_vector* P1,gsl_vector* P2){
	gsl_vector* result = gsl_vector_calloc(4);
	gsl_vector_memcpy(result,P2);
	gsl_vector_sub(result,P1);
	return result;
}

void
floatingClosestPoint(gsl_vector* FP, gsl_vector* CP, double currCylinderPos[2], gsl_matrix* DH[4], int i, int obsType)
{
	double cylinderHeight;
	double radius;
	double innerRadius;
	double safety_factor = 30;
	double testvar;
	//if(i==3) safety_factor = 5;
	
	if(obsType == 0)
	{
		cylinderHeight = 145 + safety_factor;
		innerRadius = 0;
		radius = 100/2 + safety_factor;
	}
	else if(obsType == 1)
	{
		cylinderHeight = 20;
		innerRadius = 40/2;
		radius = 68/2;
	}
	else
	{
		cylinderHeight = 125;
		innerRadius = 0;
		radius = 50/2;
	}
  gsl_vector* L1 = gsl_vector_calloc (4);
  gsl_vector* L2 = gsl_vector_calloc (4);
  gsl_vector* Cyl = gsl_vector_calloc(4);
  
  gsl_vector_set(Cyl,0,currCylinderPos[0]);
  gsl_vector_set(Cyl,1,currCylinderPos[1]);
  pointInFrame(L1, DH[i-1]);
  testvar = gsl_vector_get (L1, 0);
  testvar = gsl_vector_get (L1, 1);
  testvar = gsl_vector_get (L1, 2);
  testvar = gsl_vector_get (L1, 3);
  pointInFrame(L2, DH[i]);
  testvar = gsl_vector_get (L2, 0);
  testvar = gsl_vector_get (L2, 1);
  testvar = gsl_vector_get (L2, 2);
  testvar = gsl_vector_get (L2, 3);
  gsl_vector * L1toL2 = pointToPoint(L1,L2);
  gsl_vector* L1toL2_save = gsl_vector_calloc(4);
  gsl_vector_memcpy(L1toL2_save,L1toL2);
  gsl_vector * L2toL1 = pointToPoint(L2,L1);
 
  gsl_vector * L1toCyl = pointToPoint(L1,Cyl);
  gsl_vector * L2toCyl = pointToPoint(L2,Cyl);
  gsl_vector_set(L1toCyl,2,0);
  gsl_vector_set(L2toCyl,2,0);
  gsl_vector * L1toL2proj = gsl_vector_calloc(4);
  gsl_vector * L2toL1proj = gsl_vector_calloc(4);
  gsl_vector_memcpy(L1toL2proj,L1toL2);
  gsl_vector_set(L1toL2proj,2,0);
  gsl_vector_memcpy(L2toL1proj,L2toL1);
  gsl_vector_set(L2toL1proj,2,0);
  
  double dpl1 = gslu_vector_dot (L1toL2proj, L1toCyl);

  double dpl2 = gslu_vector_dot (L2toL1proj, L2toCyl);

  
  if(gslu_vector_dot (L1toL2proj, L1toCyl) <= 0){ gsl_vector_memcpy(FP,L1);   }//printf("Link start is FP, "); }      	
  else if(gslu_vector_dot (L2toL1proj, L2toCyl) <= 0) { gsl_vector_memcpy(FP,L2); }//printf("Link End is FP, "); }  
  else {
  	gsl_vector_memcpy(FP,L1);
  	gsl_vector_scale(L1toL2, dpl1/(gslu_vector_norm (L1toL2)*gslu_vector_norm (L1toL2proj)));
  	gsl_vector_add(FP,L1toL2);
  }

  gsl_vector_memcpy(L1toL2,L1toL2_save);  // need to reset L1toL2
  
  gsl_vector * CyltoFP = pointToPoint(Cyl,FP);
  gsl_vector_set(CyltoFP,2,0);
  double dist2Circle = gslu_vector_norm (CyltoFP);
  //printf("d2c: %f\n",dist2Circle);
  
  
  // FP goes outside of L1toL2 link
  // Doesnt check when link is inside cylinder
  
  if(dist2Circle < radius && dist2Circle > innerRadius){
  	
  	double dist=999999999;
  	if(gslu_vector_norm(L1toCyl)>radius || gslu_vector_norm(L2toCyl)>radius){
		double x1 = (gsl_vector_get(L1,0)-gsl_vector_get(Cyl,0)), y1 = (gsl_vector_get(L1,1)-gsl_vector_get(Cyl,1));
		double x2 = (gsl_vector_get(L2,0)-gsl_vector_get(Cyl,0)), y2 = (gsl_vector_get(L2,1)-gsl_vector_get(Cyl,1));
		
		double dx = x2-x1, dy=y2-y1, dr=sqrt(pow(dx,2)+pow(dy,2)), D=x1*y2-x2*y1, sgn=1;
		if(dy<0) sgn = -1;
		
		gsl_vector* radpt1 = gsl_vector_calloc(4); gsl_vector* radpt2 = gsl_vector_calloc(4);
		gsl_vector_set(radpt1,0,( D*dy+  sgn*dx*sqrt(pow(radius*dr,2)-pow(D,2)))/pow(dr,2)+gsl_vector_get(Cyl,0));
		gsl_vector_set(radpt1,1,(-D*dx+fabs(dy)*sqrt(pow(radius*dr,2)-pow(D,2)))/pow(dr,2)+gsl_vector_get(Cyl,1));
		gsl_vector_set(radpt2,0,( D*dy-  sgn*dx*sqrt(pow(radius*dr,2)-pow(D,2)))/pow(dr,2)+gsl_vector_get(Cyl,0));
		gsl_vector_set(radpt2,1,(-D*dx-fabs(dy)*sqrt(pow(radius*dr,2)-pow(D,2)))/pow(dr,2)+gsl_vector_get(Cyl,1));
		gsl_vector_set(radpt1,2,cylinderHeight);
		gsl_vector_set(radpt2,2,cylinderHeight);
		
		gsl_vector* L1toRadpt1 = pointToPoint(L1,radpt1);
		gsl_vector* L1toRadpt2 = pointToPoint(L1,radpt2);
		gsl_vector* FP1 = gsl_vector_calloc(4);
		gsl_vector_memcpy(FP1,L1);
		gsl_vector* FP2 = gsl_vector_calloc(4);
		gsl_vector_memcpy(FP2,L1);
		gsl_vector* FP1_L1toL2 = gsl_vector_calloc(4);
		gsl_vector* FP2_L1toL2 = gsl_vector_calloc(4);
		gsl_vector_memcpy(FP1_L1toL2,L1toL2);
		gsl_vector_memcpy(FP2_L1toL2,L1toL2);
  		gsl_vector_scale(FP1_L1toL2, gslu_vector_dot(L1toL2, L1toRadpt1)/(gslu_vector_norm (L1toL2)*gslu_vector_norm (L1toL2)));
  		gsl_vector_scale(FP2_L1toL2, gslu_vector_dot(L1toL2, L1toRadpt2)/(gslu_vector_norm (L1toL2)*gslu_vector_norm (L1toL2)));
  		gsl_vector_add(FP1,FP1_L1toL2);
		gsl_vector_add(FP2,FP2_L1toL2);
		double dist1 = gslu_vector_dist(FP1,radpt1);
		double dist2 = gslu_vector_dist(FP2,radpt2);
		// Need to check both radpts only when both both L1 and L2 outside circle
		if(dist1<dist2){
			gsl_vector_memcpy(FP,FP1);
			gsl_vector_memcpy(CP,radpt1);
			dist = dist1;
		} else {
			gsl_vector_memcpy(FP,FP2);
			gsl_vector_memcpy(CP,radpt2);
			dist = dist2;
		}
		
		if(sqrt(pow(x2,2)+pow(y2,2)) < radius) // L2 inside circle
		{
			if(sqrt(pow(gsl_vector_get(L1,0)-gsl_vector_get(radpt1,0),2)+pow(gsl_vector_get(L1,1)-gsl_vector_get(radpt1,1),2)) < sqrt(pow(gsl_vector_get(L1,0)-gsl_vector_get(radpt2,0),2)+pow(gsl_vector_get(L1,1)-gsl_vector_get(radpt2,1),2))) // radpt1 closer to L1
			{
				if(gsl_vector_get(L1,2) < gsl_vector_get(L2,2)) 
				{
					gsl_vector_memcpy(FP,FP1);
					gsl_vector_memcpy(CP,radpt1);
					dist = dist1;
				}
				else
				{
					gsl_vector_memcpy(FP,L2);
					gsl_vector_set(CP,0,gsl_vector_get(L2,0));
					gsl_vector_set(CP,1,gsl_vector_get(L2,1));
					gsl_vector_set(CP,2,cylinderHeight);
				}
			}
			else // radpt2 closer to L1 -> should never be equal distance
			{
				if(gsl_vector_get(L1,2) < gsl_vector_get(L2,2))
				{
					gsl_vector_memcpy(FP,FP2);
					gsl_vector_memcpy(CP,radpt2);
					dist = dist1;
				}
				else
				{
					gsl_vector_memcpy(FP,L2);
					gsl_vector_set(CP,0,gsl_vector_get(L2,0));
					gsl_vector_set(CP,1,gsl_vector_get(L2,1));
					gsl_vector_set(CP,2,cylinderHeight);
				}
			}
			//if(i==3) printf("only L2 inside\n");
		}
		if(sqrt(pow(x1,2)+pow(y1,2)) < radius) // L1 inside circle
		{
			if(sqrt(pow(gsl_vector_get(L2,0)-gsl_vector_get(radpt1,0),2)+pow(gsl_vector_get(L2,1)-gsl_vector_get(radpt1,1),2)) < sqrt(pow(gsl_vector_get(L2,0)-gsl_vector_get(radpt2,0),2)+pow(gsl_vector_get(L2,1)-gsl_vector_get(radpt2,1),2))) // radpt1 closer to L2
			{
				if(gsl_vector_get(L2,2) < gsl_vector_get(L1,2)) 
				{
					gsl_vector_memcpy(FP,FP1);
					gsl_vector_memcpy(CP,radpt1);
					dist = dist1;
				}
				else
				{
					gsl_vector_memcpy(FP,L1);
					gsl_vector_set(CP,0,gsl_vector_get(L1,0));
					gsl_vector_set(CP,1,gsl_vector_get(L1,1));
					gsl_vector_set(CP,2,cylinderHeight);
				}
			}
			else // radpt2 closer to L2 -> should never be equal distance
			{
				if(gsl_vector_get(L2,2) < gsl_vector_get(L1,2))
				{
					gsl_vector_memcpy(FP,FP2);
					gsl_vector_memcpy(CP,radpt2);
					dist = dist1;
				}
				else
				{
					gsl_vector_memcpy(FP,L1);
					gsl_vector_set(CP,0,gsl_vector_get(L1,0));
					gsl_vector_set(CP,1,gsl_vector_get(L1,1));
					gsl_vector_set(CP,2,cylinderHeight);
				}
			}
			//if(i==3) printf("only L2 inside\n");
		}
		
	}
	else // L1 and L2 both inside circle
	{
		if(gsl_vector_get(L2,2) < gsl_vector_get(L1,2))
		{
			gsl_vector_memcpy(FP,L2);
			gsl_vector_set(CP,0,gsl_vector_get(L2,0));
			gsl_vector_set(CP,1,gsl_vector_get(L2,1));
			gsl_vector_set(CP,2,cylinderHeight);
		}
		else // L2 higher than L1, can never be equal height and both be inside circle 
		{
			gsl_vector_memcpy(FP,L1);
			gsl_vector_set(CP,0,gsl_vector_get(L1,0));
			gsl_vector_set(CP,1,gsl_vector_get(L1,1));
			gsl_vector_set(CP,2,cylinderHeight);
		}
		// if L1 or L2 inside cylinder
	}
	/*
	if(gslu_vector_norm(L1toCyl)<radius || gslu_vector_norm(L2toCyl)<radius){ // what was this for?
		if(gsl_vector_get(L1,2)-cylinderHeight<dist){
			dist = gsl_vector_get(L1,2)-cylinderHeight;
			gsl_vector_memcpy(FP,L1);
			gsl_vector_memcpy(CP,FP);
			gsl_vector_set(CP, 2, cylinderHeight);
		}
		if(gsl_vector_get(L2,2)-cylinderHeight<dist){
			dist = gsl_vector_get(L2,2)-cylinderHeight;
			gsl_vector_memcpy(FP,L2);
			gsl_vector_memcpy(CP,FP);
			gsl_vector_set(CP, 2, cylinderHeight);
		}		
	}
	
	*/
	} else {
		//printf("L1 L2 outside circle\n");
		gsl_vector_memcpy(CP,Cyl);
		gsl_vector_scale(CyltoFP, radius/dist2Circle);
		gsl_vector_add(CP,CyltoFP);
		if(gsl_vector_get(FP,2)>cylinderHeight) gsl_vector_set(CP,2,cylinderHeight);
		else gsl_vector_set(CP,2,gsl_vector_get(FP,2));
	}
	
	
	//gslu_vector_printf(CP,"CP");
	gsl_vector_free(L1);
	gsl_vector_free(L2);
	gsl_vector_free(Cyl);
	gsl_vector_free(L1toCyl);
	gsl_vector_free(L2toCyl);
	gsl_vector_free(L1toL2);
	gsl_vector_free(L2toL1);
	gsl_vector_free(L1toL2proj);
	gsl_vector_free(L2toL1proj);
	gsl_vector_free(CyltoFP);
}


// passing in current config, goal point, command var, obstacle set
void
calculatePotentialCost(const double angles_initial[4], zarray_t* cylinders, zarray_t* cylinders_goal, int goal_index, zarray_t* command_buffer, zarray_t* cp_coords)
{
	// control flags for debugging
	bool attract_to_goal = true;
	bool repel_goal = true;
	bool repel_cylinders = true;
	bool repel_floor = true;
	bool repel_ceiling = true;
	
	double scale;
	double angles_command[4], angles_command_prev1[4], angles_command_prev2[4],xyzp_coords[4];
	double d = 500, rho0 = 100, norm_dist,norm_dist_CP, ceiling = 375;
	int epsilon = 0.5, stuck = 0, cmd_count=0;
	double zeta[4] = {20, 10, 10, 10}, eta[4] = {2000000, 2000000, 2000000, 2000000}, alpha[4] = {2,2,2,2};
	double rho = 0, dist_DH;
	double Urep[4], Uatt[4];
	//double Frep[4], Fatt[4];
	//double Fxtotal[4],Fytotal[4],Fztotal[4];
	double Fxatt, Fyatt,Fzatt;
	double Fxrep,Fyrep,Fzrep;
	double goal_xydist = 0;
	int i, j, k, nObstacles = zarray_size(cylinders), nGoals = zarray_size(cylinders_goal);
	double coords_prev2[4], coords_prev1[4], coords_command[4];
	int norm_dist_prev2, norm_dist_prev1, norm_dist_command;
	for(i=0;i<4;i++){
		angles_command[i] = angles_initial[i];
		angles_command_prev1[i] = 999;
		angles_command_prev2[i] = -999;
	}
	
	if(nObstacles + nGoals <= 0)
	{
		return;
	}
	gsl_matrix* DH_c[4];
	

	gsl_matrix* Jv = gsl_matrix_calloc (3, 4);
	
	

	gsl_vector* P = gsl_vector_alloc (4);
	//gsl_vector* Pf = gsl_vector_alloc (4);
	
	gsl_vector* x = gsl_vector_calloc (4);	
	gsl_vector_set (x, 3, 1.0);
	
	gsl_vector* z = gsl_vector_calloc (3);
	gsl_vector* o_diff = gsl_vector_calloc (3);
	gsl_vector* Jv_column = gsl_vector_calloc (3);
	gsl_vector* Fq = gsl_vector_calloc (3);
	gsl_vector* Torque = gsl_vector_calloc (4);
	gsl_vector* Torque_sum = gsl_vector_calloc (4);
	
	
	gsl_vector* CP = gsl_vector_alloc(4);
	gsl_vector* FP = gsl_vector_alloc(4);
	
	double normalize, shortest_distance;
	double currCylinderPos[2],currGoalPos[2], cp_xyz[3];
	zarray_get (cylinders_goal, goal_index, currGoalPos);
	double goalCoords[3] = {currGoalPos[0],currGoalPos[1],20};
	double basePos[2] = {0,0};
	
	gsl_vector* z00 = gsl_vector_calloc (3);
	gsl_vector_set (z00, 0, 0);
	gsl_vector_set (z00, 1, 0);
	gsl_vector_set (z00, 2, 1);
	bool atDestination = false;
	while(!atDestination){
		cmd_count++;
	
		calculateDHTable(angles_command,DH_c);
		gsl_vector_set_zero(Torque_sum);
		// for each link
		for(i=1;i<4;i++)
		{
			//Build Jv
			Urep[i] = 0;
			gsl_matrix_set_zero(Jv);
			// for each joint
			for(k=0;k<(i+1);k++){
				// for length of each jacobian column
				for(j=0;j<3;j++){
					if(k == 0)
					{
						gsl_vector_set (z, j, gsl_vector_get(z00,j));
					
						gsl_vector_set (o_diff, j, gsl_matrix_get(DH_c[i],j,3) - 0);
					}
					else 
					{
						gsl_vector_set (z, j, gsl_matrix_get(DH_c[k-1],j,2));
						gsl_vector_set (o_diff, j, gsl_matrix_get(DH_c[i],j,3) - gsl_matrix_get(DH_c[k-1],j,3));
					}
				}
				//gslu_vector_printf ( z, "z");
				gslu_vector_cross (Jv_column, z, o_diff);
				for(j=0;j<3;j++){
					gsl_matrix_set (Jv, j, k, gsl_vector_get(Jv_column,j));
				}
			}
			//gslu_matrix_printf(Jv,"Jv");
			//printf("num1: b->size=%d, A->size1 =%d, A->size2%d, x->size=%d",(int)P->size,(int)DH_c[i]->size1,(int)DH_c[i]->size2,(int)x->size);
			gslu_blas_mv (P, DH_c[i], x);	
			normalize = gsl_vector_get (P, 3);
			gsl_vector_set (P, 0, gsl_vector_get (P, 0)/normalize);
			gsl_vector_set (P, 1, gsl_vector_get (P, 1)/normalize);
			gsl_vector_set (P, 2, gsl_vector_get (P, 2)/normalize);
			gsl_vector_set (P, 3, 1);
		    
		    // Only attract end effector, xy stronger than z
		    if(i == 3)
		    { 
				norm_dist = sqrt(pow((gsl_vector_get (P, 0) - goalCoords[0]),2) 
						       + pow((gsl_vector_get (P, 1) - goalCoords[1]),2) 
						       + pow((gsl_vector_get (P, 2) - goalCoords[2]),2));  
		
				if(norm_dist <= d)
				{
						Uatt[i] = 0.5 * zeta[i] * pow(norm_dist, 2);
						Fxatt = -zeta[i]*(gsl_vector_get (P, 0) - goalCoords[0]);
						Fyatt = -zeta[i]*(gsl_vector_get (P, 1) - goalCoords[1]);
						Fzatt = -zeta[i]*(gsl_vector_get (P, 2) - goalCoords[2]);
				}
				else
				{
						Uatt[i] = (d * zeta[i] * norm_dist) - (0.5 * zeta[i] * pow(d,2));
						Fxatt = -d*zeta[i]*(gsl_vector_get (P, 0) - goalCoords[0])/norm_dist;
						Fyatt = -d*zeta[i]*(gsl_vector_get (P, 1) - goalCoords[1])/norm_dist;
						Fzatt = -d*zeta[i]*(gsl_vector_get (P, 2) - goalCoords[2])/norm_dist;
				
				}
		
				gsl_vector_set(Fq,0,Fxatt);
				gsl_vector_set(Fq,1,Fyatt);
				gsl_vector_set(Fq,2,Fzatt);
				gsl_blas_dgemv (CblasTrans, 1.0, Jv, Fq, 0.0, Torque);
				if(attract_to_goal) gsl_blas_daxpy (1.0, Torque, Torque_sum);
				
			}
			zarray_clear(cp_coords);
			for(j=0; j<nObstacles+nGoals+3; j++)
			{
			
				if(j<nObstacles)
				{
					zarray_get (cylinders, j, currCylinderPos); 
					floatingClosestPoint(FP,CP,currCylinderPos,DH_c,i,0);
					//printf("\nCP Obstacle[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (CP, 0),gsl_vector_get (CP, 1),gsl_vector_get (CP, 2));
					//printf("\nFP Obstacle[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (FP, 0),gsl_vector_get (FP, 1),gsl_vector_get (FP, 2));
				}
				else if(j<nObstacles+nGoals)
				{
					zarray_get (cylinders_goal, j-nObstacles, currGoalPos); 
					floatingClosestPoint(FP, CP, currGoalPos, DH_c, i, 1);
					//printf("\nCP Annulus[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (CP, 0),gsl_vector_get (CP, 1),gsl_vector_get (CP, 2));
					//printf("\nFP Annulus[q%d]: [%f,%f,%f]\n",i,gsl_vector_get (FP, 0),gsl_vector_get (FP, 1),gsl_vector_get (FP, 2));
				} else if (j<nObstacles+nGoals+2){
					gsl_vector_set(CP, 0, gsl_vector_get(P, 0));
					gsl_vector_set(CP, 1, gsl_vector_get(P, 1));
					if (j<nObstacles+nGoals+1) gsl_vector_set(CP, 2, 0);
					else {
						if(gsl_vector_get(P, 2) < ceiling) gsl_vector_set(CP, 2, ceiling);
						else gsl_vector_set(CP, 2, gsl_vector_get(P, 2) + 0.5);
					}
					
					gsl_vector_set(FP, 0, gsl_vector_get(P, 0));
					gsl_vector_set(FP, 1, gsl_vector_get(P, 1));
					gsl_vector_set(FP, 2, gsl_vector_get(P, 2));
					
				}
				else
				{
					floatingClosestPoint(FP, CP, basePos, DH_c, i, 2);
				}
				if(i==3 && (j<nObstacles+nGoals || j == nObstacles+nGoals+2))
				{
					cp_xyz[0] = gsl_vector_get (CP, 0);
					cp_xyz[1] = gsl_vector_get (CP, 1);
					cp_xyz[2] = gsl_vector_get (CP, 2);
					zarray_add(cp_coords,cp_xyz);
					if(j < nObstacles)
					{
						cp_xyz[0] = gsl_vector_get (FP, 0);
						cp_xyz[1] = gsl_vector_get (FP, 1);
						cp_xyz[2] = gsl_vector_get (FP, 2);
						zarray_add(cp_coords,cp_xyz);
					}
				}
				
				goal_xydist = sqrt(pow(goalCoords[0] - gsl_vector_get (P, 0),2) 
					       + pow(goalCoords[1] - gsl_vector_get (P, 1),2));
				rho = sqrt(pow(gsl_vector_get (CP, 0) - gsl_vector_get (FP, 0),2) 
					       + pow(gsl_vector_get (CP, 1) - gsl_vector_get (FP, 1),2)
					       + pow(gsl_vector_get (CP, 2) - gsl_vector_get (FP, 2),2));
				dist_DH = sqrt(pow(gsl_vector_get (CP, 0) - gsl_vector_get (P, 0),2) 
					       + pow(gsl_vector_get (CP, 1) - gsl_vector_get (P, 1),2)
					       + pow(gsl_vector_get (CP, 2) - gsl_vector_get (P, 2),2));
				if(dist_DH >= rho) // if DH origin is farther than FP
				{
					//printf("FP Dist: %f, DH Dist: %f",rho, dist_DH);
				}
				else
				{
					//printf("WRONG -> FP Dist: %f, DH Dist: %f",rho, dist_DH);
				}
				//printf("short dist@ q[%d]: %f\n",i,shortest_distance);
				
				printf("[q%d, obs %d]: FP %3.3f , DH %3.3f\t",i+1,j,rho, dist_DH);
				norm_dist_CP = sqrt(pow((gsl_vector_get (FP, 0) - gsl_vector_get (CP, 0)),2) 
				           + pow((gsl_vector_get (FP, 1) - gsl_vector_get (CP, 1)),2) 
				           + pow((gsl_vector_get (FP, 2) - gsl_vector_get (CP, 2)),2));
				Fxrep = 0;
				Fyrep = 0;
				Fzrep = 0;
				
				
				if((rho<=rho0) && (j!=nObstacles+nGoals || (j==nObstacles+nGoals && (goal_xydist > 13.5 || i != 3)))){
					Urep[i] += 0.5*eta[i]*pow(((1/rho) - (1/rho0)),2);
					Fxrep = eta[i]*((1/rho) - (1/rho0))*pow((1/rho),2)*(gsl_vector_get (FP, 0) - gsl_vector_get (CP, 0))/norm_dist_CP;
					Fyrep = eta[i]*((1/rho) - (1/rho0))*pow((1/rho),2)*(gsl_vector_get (FP, 1) - gsl_vector_get (CP, 1))/norm_dist_CP;
					Fzrep = eta[i]*((1/rho) - (1/rho0))*pow((1/rho),2)*(gsl_vector_get (FP, 2) - gsl_vector_get (CP, 2))/norm_dist_CP;
				}
				
				scale = 1.0;
				if(j==nObstacles+nGoals) scale = 10.0;
				
				gsl_vector_set(Fq,0,Fxrep);
				gsl_vector_set(Fq,1,Fyrep);
				gsl_vector_set(Fq,2,scale * Fzrep);
				//printf("Frep@ q[%d] = [%7.3f,%7.3f,%7.3f]\n",i,Fxrep,Fyrep,Fzrep);
				gsl_blas_dgemv (CblasTrans, 1.0, Jv, Fq, 0.0, Torque);
				if((repel_goal && j<nObstacles+nGoals && j>=nObstacles) || (repel_cylinders && j<nObstacles) || (repel_floor && j==nObstacles+nGoals) || (repel_ceiling && j==nObstacles+nGoals+1))	gsl_blas_daxpy (1.0, Torque, Torque_sum);
			}
			printf("\n");
		
		
			//Fxtotal[i] = Fxatt[i] + Fxrep[i];
			//Fytotal[i] = Fyatt[i] + Fyrep[i];
			//Fztotal[i] = Fzatt[i] + Fzrep[i];
		
			//printf("\nUrep@ q[%d] = %f\n",i,Urep[i]);
			
			//printf("Uatt@ q[%d] = %f\n",i,Uatt[i]);
			//printf("Fatt@ q[%d] = [%f,%7.3f,%7.3f]\n",i,Fxatt[i],Fyatt[i],Fzatt[i]);
			//printf("\nFtotal@ q[%d] = [%7.3f,%7.3f,%7.3f]\n",i,Fxtotal[i],Fytotal[i],Fztotal[i]);
		
			//printf("Pos: [%7.3f,%7.3f,%7.3f], Goal: [%7.3f,%7.3f,%7.3f], D:%7.3f\n\n",gsl_vector_get (P, 0),gsl_vector_get (P, 1),gsl_vector_get (P, 2),goalCoords[0],goalCoords[1],goalCoords[2],norm_dist);
		}
		
		printf("\n");
		//gslu_vector_printf ( Torque_sum, "Torque Sum");
		if(gsl_blas_dnrm2(Torque_sum) != 0)	gsl_vector_scale(Torque_sum, 1.0/gsl_blas_dnrm2(Torque_sum));
		//gslu_vector_printf ( Torque_sum, "Torque Sum");
		//Descend gradient
		// check if distance to goal is decreasing
		//for each joint
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		//printf("Delta angles: ");
		stuck = 0;
		for(i=0;i<4;i++){
			angles_command_prev2[i] = angles_command_prev1[i];
			angles_command_prev1[i] = angles_command[i];
			angles_command[i] += alpha[i] * gsl_vector_get(Torque_sum,i);
			
			if((abs(angles_command_prev2[i] - angles_command[i]) <= epsilon)) stuck++;
			//printf("%7.3f, ",(angles_command[i]-angles_command_prev1[i]));
		}
		//printf("\n");
		calculateCoord(angles_command_prev2, coords_prev2);
		calculateCoord(angles_command_prev1, coords_prev1);
		calculateCoord(angles_command, coords_command);
		norm_dist_prev2 = sqrt(pow((coords_prev2[0] - goalCoords[0]),2) 
						       + pow((coords_prev2[1] - goalCoords[1]),2) 
						       + pow((coords_prev2[2] - goalCoords[2]),2));
       	norm_dist_prev1 = sqrt(pow((coords_prev1[0] - goalCoords[0]),2) 
						       + pow((coords_prev1[1] - goalCoords[1]),2) 
						       + pow((coords_prev1[2] - goalCoords[2]),2));
       	norm_dist_command = sqrt(pow((coords_command[0] - goalCoords[0]),2) 
						       + pow((coords_command[1] - goalCoords[1]),2) 
						       + pow((coords_command[2] - goalCoords[2]),2));
		
		if(command_buffer != NULL){
			if(norm_dist_command < norm_dist_prev1 && norm_dist_command <= norm_dist_prev2 ) {
				if(command_buffer != NULL) zarray_add (command_buffer, angles_command);
			}
			else {
				bool valid_path = false;
				double random_destination[4];
				printf("Stuck, random walking\n");
				while(!valid_path){
					for(i=0;i<4;i++) random_destination[i] = angles_command[i] + (rand()%200 - 100)/60.0;
					valid_path = interpolateCubic(angles_command,random_destination,NULL,cylinders,cylinders_goal,NULL);
				}
				for(i=0;i<4;i++) angles_command[i] = random_destination[i];
				if(command_buffer != NULL) zarray_add (command_buffer, angles_command);
				printf("random path found\n");
			}
			calculateCoord(angles_command,xyzp_coords);
			atDestination = xyzThreshold(xyzp_coords, goalCoords);
			if(atDestination) printf("\nGoal command sent on cmd #%d, returning\n",cmd_count);
		} else atDestination = true; // quit after one loop for measure mode
	
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
	}

}

void
constructRoadMap(const int nNodes, const int kClosest, zarray_t* cylinders, zarray_t* cylinders_goal, dijkstra_graph_t* roadMap, const double q_initial[4], zarray_t* command_buffer, zarray_t* prmPoints, zarray_t *cubicPath)
{
	zarray_clear(prmPoints);
	zarray_clear(cubicPath);
	int currNodesNum, nObstacles = zarray_size(cylinders), nGoals = zarray_size(cylinders_goal);
	if(nObstacles + nGoals <= 0)
	{
		printf("No Obstacles of Goals placed.\n");
		return;
	}
	int i=0, j, k, nConnected;
	currNodesNum = dijkstra_n_nodes (roadMap);
	printf("# nodes: %d\n",currNodesNum);
	double normalize, shortest_distance, dist, eps = 10.0;
	double currCylinderPos[2], currGoalPos[2], distnIndex[2], coords_curr[4];
	double *q_curr, *q_ref, *q_dest;
	bool pathDNE, goodEdge, goodConfig = true; // path does not exist
	gsl_vector* q_curr_vec = gsl_vector_alloc (4);
	gsl_vector* q_ref_vec = gsl_vector_alloc (4);
	
	size_t pair_sz = sizeof(double[2]);
    zarray_t* distance_list = zarray_create (pair_sz);
	// generate uniform sampling of configurations
	int q1, q2, q3, q4, q_sample_i = 0, q_get = 0;
	double q_add[4];
	size_t q_sz = sizeof(double[4]);
	zarray_t* q_sample = zarray_create (q_sz);
	for(q1=-180;q1<=180;q1=q1+10)
	{
		for(q2=-120;q2<=120;q2=q2+24)
		{
			for(q3=-120;q3<=120;q3=q3+24)
			{
				for(q4=-120;q4<=120;q4=q4+24)
				{
					q_add[0] = q1; q_add[1] = q2; q_add[2] = q3; q_add[3] = q4;
					zarray_add (q_sample, q_add);
				}
			}
		}
	}
	
	
	// Forward Kinematics Setup
	gsl_vector* P = gsl_vector_alloc(3);	
	gsl_vector* CP = gsl_vector_alloc(3);
	
	// directly from pseudocode
	// i -> current number of collision free configurations in roadmap graph
	while(i < nNodes - nGoals)
	{
		// grab a configuration from uniform sampling and store in q_curr
		q_curr = (double*)malloc(4*sizeof(double));
		q_curr[0] = 180 + rand() / (RAND_MAX / (-180 - (180) + 1) + 1); 
		q_curr[1] = 120 + rand() / (RAND_MAX / (-120 - (120) + 1) + 1);
		q_curr[2] = 120 + rand() / (RAND_MAX / (-120 - (120) + 1) + 1);
		q_curr[3] = 120 + rand() / (RAND_MAX / (-120 - (120) + 1) + 1);
		//zarray_get (q_sample, q_get, q_curr); 
		//q_get++;
		// Calculate x,y,z from q_curr and store into gsl_vector P
		calculateCoord(q_curr, coords_curr);	
		gsl_vector_set (P, 0, coords_curr[0]);
		gsl_vector_set (P, 1, coords_curr[1]);
		gsl_vector_set (P, 2, coords_curr[2]);
		
        if(isCollisionFree(q_curr, cylinders, cylinders_goal))
        {
        	// add q_curr to graph
        	dijkstra_set_user (roadMap, i, q_curr);
        	zarray_add (prmPoints, coords_curr);
        	i++;
        }
	
	} // end loop for adding <nNodes> collision-free nodes
	for(i=0;i<nGoals;i++)
	{
		zarray_get (cylinders_goal, i, currGoalPos);
		q_curr = (double*)malloc(4*sizeof(double));
		q_curr[0] = currGoalPos[0];
		q_curr[1] = currGoalPos[1];
		q_curr[2] = 30;
		dijkstra_set_user (roadMap, nNodes-nGoals + i, q_curr);
		calculateCoord(q_curr, coords_curr);
        zarray_add (prmPoints, coords_curr);
	}
	// i -> each configuration in roadMap
	for(i=0;i<nNodes;i++)
	{
		//printf("i=%d\n",i);
		// convert q[4] to vector
		q_ref = dijkstra_get_user (roadMap, i);
		gsl_vector_set (q_ref_vec, 0, q_ref[0]);
		gsl_vector_set (q_ref_vec, 1, q_ref[1]);
		gsl_vector_set (q_ref_vec, 2, q_ref[2]);
		gsl_vector_set (q_ref_vec, 3, q_ref[3]);
		// get k closest neigbors
		// j -> each configuration
		
		for(j=0;j<nNodes;j++)
		{
			// convert q[4] to vector
			q_curr = dijkstra_get_user (roadMap, j);
			
			gsl_vector_set (q_curr_vec, 0, q_curr[0]);
			gsl_vector_set (q_curr_vec, 1, q_curr[1]);
			gsl_vector_set (q_curr_vec, 2, q_curr[2]);
			gsl_vector_set (q_curr_vec, 3, q_curr[3]);
			//printf("i=%d, j=%d\n",i,j);
			//gslu_vector_printf ( q_ref_vec, "q_ref_vec");
			//gslu_vector_printf ( q_curr_vec, "q_curr_vec");
			dist = gslu_vector_dist (q_ref_vec, q_curr_vec);
			
			if(dist != 0)
			{
				distnIndex[0] = j;
				distnIndex[1] = dist;
				zarray_add (distance_list, distnIndex);
			}
		} // end loop for checking distance to each q
		zarray_sort (distance_list, zdistcmp);

		nConnected = 0;
		// k -> each configuration of the closest neighbors to current configuration, i
		printf("i=%d\n",i);
//		for(k=1;k<=10;k++){
//			if(i >= k*nNodes/10.0 && i <= (k*nNodes/10.0)+1) printf("%d0 percent",k);
//		}
		for(k=0;k<nNodes;k++)
		{
			zarray_get (distance_list, k, distnIndex);
			q_curr = dijkstra_get_user (roadMap, (int)(distnIndex[0]));
			//printf("i=%d, k=%d\n",i, k);
			if(dijkstra_get_edge_weight (roadMap, i, (int)distnIndex[0]) < 0)
			{
				goodEdge = interpolateCubic(q_ref,q_curr,NULL,cylinders,cylinders_goal, NULL);
				//usleep(1000000);
				if(goodEdge)
				{
				// use cubic spline to make path for edge
					dijkstra_add_edge_undir (roadMap, i, (int)distnIndex[0], distnIndex[1]);
					nConnected++;
				}
				//else printf("Bad edge: %d to %d\n",i,(int)distnIndex[0]);
				else
				{
					// Use Potential Field Planner
				}
			}
			if(nConnected >= kClosest) break;
		}
		zarray_clear (distance_list); // clear before making new list for next node
	} // end loop for adding edges
	
	
	
	
	
	// adding q_init and q_final
	q_curr = (double*)malloc(4*sizeof(double));
	q_curr[0] = q_initial[0];
	q_curr[1] = q_initial[1];
	q_curr[2] = q_initial[2];
	q_curr[3] = q_initial[3];
	// set index for node containing q_init to nNodes
	dijkstra_set_user (roadMap, nNodes, q_curr);
	double final_coords[4];
	double angles_opt[16];
	double q_goal[4];// q_prev[4];
	for(i=0;i<nGoals;i++)
	{
		zarray_get (cylinders_goal, i, currGoalPos);
		
		// how do I get goal configuration -> reverse kinematics needs to specify pitch
		final_coords[0] = currGoalPos[0]; final_coords[1] = currGoalPos[1]; final_coords[2] = 30;
		//if(sqrt(pow(final_coords[0],2)+pow(final_coords[1],2)) < 202)
		final_coords[3] = 100;
		goodConfig = calculateAngles(final_coords, angles_opt);
		printf("goodConfig = %d\n",goodConfig);
		if(!goodConfig)
		{
			for(j=0;j<90;j+=5)
			{
				final_coords[3] = 135-j;
				goodConfig = calculateAngles(final_coords, angles_opt);
				if(goodConfig) break;
			}
			if(!goodConfig)
			{
				printf("Goal Unreachable\n");
			}
		}
		/*
		if(i == 0)
		{
			chooseYaw(angles_opt, q_initial, q_goal);
			q_prev[0] = q_goal[0];
			q_prev[1] = q_goal[1];
			q_prev[2] = q_goal[2];
			q_prev[3] = q_goal[3];
			
		}
		else
		{
			chooseYaw(angles_opt, q_prev, q_goal);
			q_prev[0] = q_goal[0];
			q_prev[1] = q_goal[1];
			q_prev[2] = q_goal[2];
			q_prev[3] = q_goal[3];
		}
		*/
		if(goodConfig)	chooseYaw(angles_opt, q_initial, q_goal);
		else
		{
			q_goal[0] = q_initial[0];
			q_goal[1] = q_initial[1];
			q_goal[2] = q_initial[2];
			q_goal[3] = q_initial[3];
		}
		q_curr = (double*)malloc(4*sizeof(double));
		q_curr[0] = q_goal[0];
		q_curr[1] = q_goal[1];
		q_curr[2] = q_goal[2];
		q_curr[3] = q_goal[3];
		// set index for node containing each q_goal starting at nNodes+1+i
		dijkstra_set_user (roadMap, nNodes+1+i, q_curr);
		
		// find closest neighbor for q_goal that can be connected
		gsl_vector_set (q_ref_vec, 0, q_goal[0]);
		gsl_vector_set (q_ref_vec, 1, q_goal[1]);
		gsl_vector_set (q_ref_vec, 2, q_goal[2]);
		gsl_vector_set (q_ref_vec, 3, q_goal[3]);
		for(j=0;j<nNodes;j++)
		{
			// convert q[4] to vector
			q_curr = dijkstra_get_user (roadMap, j);
			gsl_vector_set (q_curr_vec, 0, q_curr[0]);
			gsl_vector_set (q_curr_vec, 1, q_curr[1]);
			gsl_vector_set (q_curr_vec, 2, q_curr[2]);
			gsl_vector_set (q_curr_vec, 3, q_curr[3]);
		
			dist = gslu_vector_dist (q_ref_vec, q_curr_vec);
			distnIndex[0] = j;
			distnIndex[1] = dist;
			zarray_add (distance_list, distnIndex); 
		} // end loop for checking distance to each q
		
		zarray_sort (distance_list, zdistcmp);
		pathDNE = true;
		// k -> each configuration of the closest neighbors to current configuration, i
		for(k=0;k<nNodes;k++)
		{
			zarray_get (distance_list, k, distnIndex);
			// index of each goal is nNodes+i
			if(dijkstra_get_edge_weight (roadMap, nNodes+1+i, distnIndex[0]) < 0)// && interpolateCubic(q_goal,q_curr,NULL,cylinders,cylinders_goal))
			{
				dijkstra_add_edge_undir (roadMap, nNodes+1+i, (int)distnIndex[0], distnIndex[1]);
				pathDNE = false;
				break; // if edge can be connected then we have connected q_init
			}
		}
		if(pathDNE)
		{
			printf("q_goal[%d] can not be connected\n",i);
		}
		zarray_clear (distance_list); // clear before making new list for next node
	}
	
	// find closest neighbor for q_init that can be connected
	gsl_vector_set (q_ref_vec, 0, q_initial[0]);
	gsl_vector_set (q_ref_vec, 1, q_initial[1]);
	gsl_vector_set (q_ref_vec, 2, q_initial[2]);
	gsl_vector_set (q_ref_vec, 3, q_initial[3]);
	for(j=0;j<nNodes;j++)
	{
		// convert q[4] to vector
		q_curr = dijkstra_get_user (roadMap, j);
		gsl_vector_set (q_curr_vec, 0, q_curr[0]);
		gsl_vector_set (q_curr_vec, 1, q_curr[1]);
		gsl_vector_set (q_curr_vec, 2, q_curr[2]);
		gsl_vector_set (q_curr_vec, 3, q_curr[3]);
		
		dist = gslu_vector_dist (q_ref_vec, q_curr_vec);
		distnIndex[0] = j;
		distnIndex[1] = dist;
		zarray_add (distance_list, distnIndex); 
	} // end loop for checking distance to each q
	
	zarray_sort (distance_list, zdistcmp);
	pathDNE = true;
	
	// k -> each configuration of the closest neighbors to current configuration, i
	for(k=0;k<nNodes;k++)
	{
		zarray_get (distance_list, k, distnIndex);
		q_curr = dijkstra_get_user (roadMap, distnIndex[0]);
		if(dijkstra_get_edge_weight (roadMap, nNodes, distnIndex[0]) < 0 && interpolateCubic(q_initial,q_curr,NULL,cylinders,cylinders_goal, NULL))
		{
			dijkstra_add_edge_undir (roadMap, nNodes, (int)distnIndex[0], distnIndex[1]);
			pathDNE = false;
			break; // if edge can be connected then we have connected q_init
		}
		
	}
	if(pathDNE)
	{
		printf("q_initial can not be connected");
		return;
	}
	zarray_clear (distance_list); // clear before making new list for next node
	int q_init_closest, q_goal_closest;
	
	for(i=0;i<dijkstra_n_nodes (roadMap);i++)
	{
		q_curr = dijkstra_get_user (roadMap, i);
		if(i<nNodes)
		{
			//printf("q%d :[%f,%f,%f,%f]\n",i,q_curr[0],q_curr[1],q_curr[2],q_curr[3]);
		}
		else if(i<nNodes+1)
		{
			printf("q_init%d :[%f,%f,%f,%f]\n",i,q_curr[0],q_curr[1],q_curr[2],q_curr[3]);
			for(j=0;j<nNodes+nGoals+1;j++)
			{
				if(dijkstra_get_edge_weight (roadMap, i, j) >= 0)
				{
					printf("\t Edge %d-%d : %f\n",i,j,dijkstra_get_edge_weight (roadMap, i, j));
					q_init_closest = j;
				}
			}
		}
		else
		{
			printf("q_goal%d :[%f,%f,%f,%f]\n",i,q_curr[0],q_curr[1],q_curr[2],q_curr[3]);
			for(j=0;j<nNodes+nGoals+1;j++)
			{
				if(dijkstra_get_edge_weight (roadMap, j, i) >= 0)
				{
					printf("\t Edge %d-%d : %f\n",j,i,dijkstra_get_edge_weight (roadMap, j, i));
					q_goal_closest = j;
				}
			}	
		}
	}
	int pLen=1;
	int *shortest_path = NULL;
	double *shortest_path_dist = NULL;
	
	// calculate shortest path to first goal
	// q_init on index nNodes, first q_goal on nNodes+1
	for(i=0;i<nGoals;i++)
	{
		dijkstra_calc_dest (roadMap, nNodes, nNodes+1+i);
		dijkstra_print_path (roadMap, nNodes, nNodes+1+i);	printf("\n");
		pLen = dijkstra_get_path (roadMap, nNodes, nNodes+1+i, &shortest_path, &shortest_path_dist);
		for(j=0;j<pLen-1;j++)
		{
			q_curr = dijkstra_get_user (roadMap, shortest_path[j]);
			q_dest = dijkstra_get_user (roadMap, shortest_path[j+1]);
			printf("q_waypoint %d :[%f,%f,%f,%f]\n",j,q_dest[0],q_dest[1],q_dest[2],q_dest[3]);
			interpolateCubic(q_curr,q_dest,NULL,cylinders,cylinders_goal, cubicPath);
		}
		// USE PF planner to go from nearest node to goal
		//calculatePotentialCost(q_dest, cylinders, cylinders_goal, i, command_buffer);
		for(j=pLen-1;j>0;j--)
		{
			q_curr = dijkstra_get_user (roadMap, shortest_path[j]);
			q_dest = dijkstra_get_user (roadMap, shortest_path[j-1]);
			interpolateCubic(q_curr,q_dest,NULL,cylinders,cylinders_goal, cubicPath);
		}
		
		
	}
	
	
} // end function

void
planPRM(const int nNodes, const int kClosest, zarray_t* cylinders, zarray_t* cylinders_goal, dijkstra_graph_t* roadMap, const double q_initial[4], zarray_t* command_buffer, zarray_t *cubicPath)
{
	int pLen,i,j;
	int *shortest_path = NULL;
	double *shortest_path_dist = NULL;
	double *q_curr, *q_dest, q_goal[4];
	// calculate shortest path to first goal
	// q_init on index nNodes, first q_goal on nNodes+1
	for(i=0;i<zarray_size(cylinders_goal);i++)
	{
		dijkstra_calc_dest (roadMap, nNodes, nNodes+1+i);
		dijkstra_print_path (roadMap, nNodes, nNodes+1+i);	printf("\n");
		pLen = dijkstra_get_path (roadMap, nNodes, nNodes+1+i, &shortest_path, &shortest_path_dist);
		for(j=0;j<pLen-1;j++)
		{
			q_curr = dijkstra_get_user (roadMap, shortest_path[j]);
			q_dest = dijkstra_get_user (roadMap, shortest_path[j+1]);
			printf("q_waypoint %d :[%f,%f,%f,%f]\n",j,q_dest[0],q_dest[1],q_dest[2],q_dest[3]);
			interpolateCubic(q_curr,q_dest,command_buffer,cylinders,cylinders_goal, NULL);
		}
		for(j=0;j<2;j++) interpolateCubic(q_dest,q_dest,command_buffer,cylinders,cylinders_goal, NULL);
		// USE PF planner to go from nearest node to goal
		//calculatePotentialCost(q_dest, cylinders, cylinders_goal, i, command_buffer);
		double xyz[3];
		zarray_get(cylinders_goal, i, xyz);
		double final_coords[4];
		double angles_opt[16];
		final_coords[0] = xyz[0]; final_coords[1] = xyz[1]; final_coords[2] = 10; final_coords[3] = 100;
		bool goodConfig = calculateAngles(final_coords, angles_opt);
		//printf("goodConfig = %d\n",goodConfig);
		int j;
		if(!goodConfig)
		{
		        
			for(j=0;j<90;j+=5)
			{
				final_coords[3] = 135-j;
				goodConfig = calculateAngles(final_coords, angles_opt);
				if(goodConfig) break;
			}
			if(!goodConfig)
			{
				printf("Goal Unreachable\n");
			}
		}
		
		if(goodConfig) 
		{
			chooseYaw(angles_opt,q_dest, q_goal);		
			//interpolateCubic(q_dest,q_curr,command_buffer,cylinders,cylinders_goal);
			zarray_add (command_buffer, q_goal);
			for(j=0;j<5;j++) interpolateCubic(q_goal,q_goal,command_buffer,cylinders,cylinders_goal, NULL);
			//interpolateCubic(q_curr,q_dest,command_buffer,cylinders,cylinders_goal);
			zarray_add (command_buffer, q_dest);
			for(j=0;j<2;j++) interpolateCubic(q_dest,q_dest,command_buffer,cylinders,cylinders_goal, NULL);
			for(j=pLen-1;j>0;j--)
			{
				q_curr = dijkstra_get_user (roadMap, shortest_path[j]);
				q_dest = dijkstra_get_user (roadMap, shortest_path[j-1]);
				printf("q_waypoint %d :[%f,%f,%f,%f]\n",j,q_dest[0],q_dest[1],q_dest[2],q_dest[3]);
				interpolateCubic(q_curr,q_dest,command_buffer,cylinders,cylinders_goal, NULL);
			}
			for(j=0;j<5;j++) interpolateCubic(q_dest,q_dest,command_buffer,cylinders,cylinders_goal, NULL);
		}
	}

} // end function


int
zdistcmp (const void *a_pp, const void *b_pp)
{
    assert (a_pp != NULL);
    assert (b_pp != NULL);

    double *a = (void*)a_pp;
    double *b = (void*)b_pp;
    double diff = b[1]-a[1];
	if(diff>0) return -1;
	else if(diff<0) return 1;
	else return 0;
}



