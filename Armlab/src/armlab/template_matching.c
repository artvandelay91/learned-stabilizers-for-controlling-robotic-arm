#include "template_matching.h"
#include <limits.h>
#include <stdlib.h>

unsigned long tolerance = 3000; 

void getMatchLocations(image_u32_t * img, image_u32_t * temp_img, int * numMatches, zarray_t *obsList ){

  printf("%d, %d\n", img->width, img->height);
  printf("%d, %d\n", temp_img->width, temp_img->height);
  
  unsigned long *err_arr, *sup_arr1, *sup_arr11, *sup_arr2;

	int temp_height, temp_width, q_height, q_width, q_stride, t_stride;
	int totObjs;

	q_height = img->height;
	q_width = img->width;
	q_stride = img->stride;

	
	//(Trajectory_t*)calloc(1,sizeof(Trajectory_t));
	err_arr = (unsigned long*)calloc(q_height*q_stride, sizeof(unsigned long));
	sup_arr1 = (unsigned long*)calloc(q_height*q_stride, sizeof(unsigned long));
	//sup_arr11 = (unsigned long*)calloc(q_height*q_stride, sizeof(unsigned long));

	sup_arr2 = (unsigned long*)calloc(q_height*q_stride, sizeof(unsigned long));
        printf("here1\n");
	for(int i=0;i< q_height*q_stride; i++){
	  err_arr[i]= ULONG_MAX;
	  sup_arr1[i] = ULONG_MAX;
	  //sup_arr11[i] = ULONG_MAX;
	  sup_arr2[i] = ULONG_MAX;
	}

	//printf("Matching template \n");

	templateMatch(img,temp_img,err_arr);

	//printf("Minima supression \n");

	int window_size_y = temp_img->height*3/4;
        int window_size_x = temp_img->width*3/4;


        minimaSupression(0,0,window_size_x,window_size_y,img,err_arr,sup_arr1,tolerance);                           
        minimaSupression(window_size_x/2,window_size_y/2,window_size_x,window_size_y,img, sup_arr1, sup_arr2,tolerance);   

	//printf("Getting number of object matches \n");

	totObjs = 0;
	for (int i = 0; i < q_height*q_stride; i++){
		if (sup_arr2[i] != ULONG_MAX)
			totObjs++;
		
	}

	//printf("Total objects = %d \n",totObjs);

	//printf("Getting object co-ordinates \n");

        int pos[2];
	for (int i = 0; i < q_height*q_stride; i++){
	  if (sup_arr2[i] != ULONG_MAX){
	    int x = i % q_stride;
	    int y = floor(i / q_stride);
	    
	    pos[0] = x; pos[1] =  y;
	    zarray_add(obsList, pos);
	  }
	}
	
	
}


void updateArr(unsigned long *arr,unsigned long newValue){
  unsigned long maxArr= 0;
  int maxInx; 
  for(int i = 0;i<20;i++){
    if(maxArr < arr[i]){
      maxArr = arr[i];
      maxInx = i;
    }
   }
  if(maxArr >newValue)
    arr[maxInx] = newValue;
}




void minimaSupression(int init_x,int init_y,int  window_size_x,int window_size_y, image_u32_t * img,  unsigned long *arr, unsigned long *sup_arr,int tolerance){

  //int init_i = 0;
  //int init_j = 0;

	int q_height, q_width, q_stride;
	
	q_height = img->height;
	q_width = img->width;
	q_stride = img->stride;

	unsigned long local_min;
	int  n_min, m_min;

	unsigned long global_min = ULONG_MAX;

	unsigned long local_min_arr[20];


	for(int leastInx = 0;leastInx <20;leastInx++)
	  local_min_arr[leastInx] = ULONG_MAX;


	for (int i = init_y; i + window_size_y <= q_height; i = i + window_size_y){
	  for (int j = init_x; j + window_size_x <= q_width; j = j + window_size_x){ // <= because window_size is a width
	    local_min = ULONG_MAX;
	    n_min = 0;
	    m_min = 0;
	    for (int n = i; n < i + window_size_y; n++){
	      for (int m = j; m < j + window_size_x; m++){
		
		if (local_min > arr[n*q_stride + m]){
		  n_min = n;
						m_min = m;
						local_min = arr[n*q_stride + m];
		}
		
	      }
	    }

#ifdef DEBUG
	    //printf("Local Min = %lu \n",local_min);
#endif


	    updateArr(local_min_arr,local_min);
	    
	    if(global_min > local_min)
	      global_min = local_min;
	    
	    if(local_min < tolerance)
	      sup_arr[n_min*q_stride + m_min] = local_min;
			
	  }
	}
	
#ifdef DEBUG
	for(int leastInx = 0;leastInx <20;leastInx++)
	  printf("Least Loacl Min = %lu \n",local_min_arr[leastInx]);
	
	printf("Gobal Min = %lu \n",global_min);
#endif

	
}



void templateMatch(image_u32_t * img, image_u32_t * temp_img, unsigned long *err_arr){

  int temp_height, temp_width, q_height, q_width, q_stride, t_stride;
  
  uint32_t *temp_buf,*img_buf;
	
  
  temp_height = temp_img->height;
  temp_width = temp_img->width;
  t_stride = temp_img->stride;
  temp_buf = temp_img->buf;
  
  q_height = img->height;
  q_width = img->width;
  q_stride = img->stride;
  img_buf = img->buf;
  
  int temp_height_2 = temp_height / 2;
  int temp_width_2 = temp_width / 2;
  
  
  for (int qy = 0; qy + temp_height < q_height; qy++){
    for (int qx = 0; qx + temp_width < q_width; qx++){
      
      unsigned long err = 0;
      
      for (int ty = 0; ty < temp_height; ty++){
	for (int tx = 0; tx < temp_width; tx++){
	  //err += compute_err(img_buf[(qy + ty)*q_stride + qx + tx], temp_buf[ty*t_stride + tx]);
	  err += compute_err(img_buf[ (qy + ty)*q_stride + qx + tx], temp_buf[ty*t_stride + tx]);
	}
      }
      
      err_arr[(qy + temp_height_2)*q_stride + (qx + temp_width_2)] = err/(temp_height*temp_width);
      //temp_height_2 and temp_width_2 included so as to bring the err pixel close to the center of the image cordinates.
      
#ifdef DEBUG
      //printf("Err = %lu,, qy =  %d, qx =  %d \n",err,qy,qx);
      //printf("img height = %d,img widht =  %d, qy =  %d, qx =  %d \n",q_height,q_width,qy,qx);
#endif
      
    }
  }
  
}



int getMax(int a, int b, int c){
  
  if (a > b){
    if (a > c) return a;
    else return c;
  }
  
  if (b > c) return b;
  else return c;
  
}



int getMin(int a, int b, int c){
  
  if (a < b){
    if (a < c) return a;
    else return c;
  }
  
  if (b < c) return b;
  else return c;
  
}


void splitIntoComponents(uint32_t buff, uint8_t *comp){
  comp[0] = (buff >> 0) & 0xff;
  comp[1] = (buff >> 8) & 0xff;
  comp[2] = (buff >> 16) & 0xff;
  comp[3] = (buff >> 24) & 0xff;
}


int compute_err(uint32_t buff_q, uint32_t buff_t){
  
  int rq, gq, bq, rt, gt, bt;//aq,at;
  uint8_t comp[4];
  int Cmax_i, Cmax_t,Cmin_i, Cmin_t;
  double S_i, S_t;
  int a_r,a_g,a_b, a_s;
  int err=0;
  int err_s = 0;
  static int const th = 64;

  splitIntoComponents(buff_q, comp);
  rq = (int) comp[0];
  gq = (int) comp[1];
  bq = (int) comp[2];
  //aq = (int) comp[3];
  
  
  splitIntoComponents(buff_t, comp);
  rt = (int) comp[0];
  gt = (int) comp[1];
  bt = (int) comp[2];
  //at = (int) comp[3];
  
  
  a_r = abs(rq - rt);
  a_g = abs(gq - gt);
  a_b = abs(bq - bt);
  
  
  //hybrid
  if(a_r <= th)
    err += pow(a_r,2);
  else
    err += pow(th,2)+a_r;
  
  if(a_g <= th)
    err += pow(a_g,2);
  else
    err += pow(th,2)+a_g;
  
  if(a_b <= th)
    err += pow(a_b,2);
  else
    err += pow(th,2)+a_b;
  
  //HSV - S Error;
  Cmax_i = getMax(rq, gq, bq);
  Cmax_t = getMax(rt, gt, bt);
  
  Cmin_i = getMin(rq, gq, bq);
  Cmin_t = getMin(rt, gt, bt);
  
  if (Cmax_i == 0)
    S_i = 0;
  else
    S_i = (Cmax_i - Cmin_i)*500 / Cmax_i;
  
  if (Cmax_t == 0)
    S_t = 0;
  else
    S_t = (Cmax_t - Cmin_t)*500 / Cmax_t;
  
  //S hybrid
  
  a_s = abs(S_i - S_t);
  
  if(a_s <= 250)
    err_s = pow(a_s,2);
  else
    err_s = pow(250,2)+a_s;
  
  
  
  
  
#ifdef DEBUG
	//printf("Err pixel HYBRID: %d \n", err);
	//printf("Err pixel S: %d \n", err_s);
	//printf("Abs Err pixel: %d + %d + %d = %d\n",abs(rq - rt) , abs(gq -gt) , abs(bq - bt),abs(rq - rt) + abs(gq -gt) + abs(bq - bt));
#endif

	//return pow((rq+gq+bq)-(rt+gt+bt),2);


  return (int)(err+err_s)/2;	
	//return err;
}

/*
	HSV - S Error;
	Cmax_i = getMax(rq, gq, bq);
	Cmax_t = getMax(rt, gt, bt);

	Cmin_i = getMin(rq, gq, bq);
	Cmin_t = getMin(rt, gt, bt);

	if (Cmax_i == 0)
		S_i = 0;
	else
	  S_i = (Cmax_i - Cmin_i)*1000 / Cmax_i;

	if (Cmax_t == 0)
		S_t = 0;
	else
	  S_t = (Cmax_t - Cmin_t)*1000 / Cmax_t;


	return (int) pow((S_i - S_t),2);
*/



