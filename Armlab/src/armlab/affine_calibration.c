#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <math.h>

#include "affine_calibration.h"

affine_calibration_t *
affine_calibration_create (const zarray_t *pairs)
{
	// create and calculate matrix
	
	affine_calibration_t *cal = calloc (1, sizeof(*cal));
	affine_pair_t *affine_pair = calloc (1, sizeof(*affine_pair));
	cal->AMatrix = gsl_matrix_alloc (2*zarray_size(pairs), 6);
	cal->worldVector = gsl_vector_alloc (2*zarray_size(pairs));
	cal->solVector = gsl_vector_alloc (6);
	cal->calMatrix = gsl_matrix_alloc (3, 3);
	gsl_matrix_set_zero (cal->AMatrix);
	gsl_matrix_set_identity (cal->calMatrix);
	int i;
	for(i=0;i<2*zarray_size(pairs);i++)
	{
		zarray_get (pairs, floor(i/2.0), affine_pair);
		gsl_matrix_set (cal->AMatrix,i,3*(i%2),affine_pair->pixel[0]); 
		gsl_matrix_set (cal->AMatrix,i,(3*(i%2))+1,affine_pair->pixel[1]);
		gsl_matrix_set (cal->AMatrix,i,(3*(i%2))+2,1);
		gsl_vector_set (cal->worldVector, i, affine_pair->world[i%2]);
	}
	saveMatrix(cal->AMatrix, "AMatrix.txt");
	saveVector(cal->worldVector, "worldVector.txt");
	/*
	gsl_vector *tau = gsl_vector_alloc (6);
	gsl_vector *residual = gsl_vector_alloc (2*zarray_size(pairs));
	gsl_linalg_QR_decomp (cal->AMatrix, tau);
	gsl_linalg_QR_lssolve (cal->AMatrix,tau, cal->worldVector, cal->solVector, residual);
	saveVector(cal->solVector, "solVector.txt");
	
	*/
	gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (2*zarray_size(pairs),6);
	gsl_matrix * cov = gsl_matrix_calloc(6,6);
	double chisq;
	gsl_multifit_linear (cal->AMatrix, cal->worldVector, cal->solVector, cov, &chisq, work);
	
	
	gsl_matrix_set (cal->calMatrix, 0, 0, gsl_vector_get (cal->solVector, 0));
	gsl_matrix_set (cal->calMatrix, 0, 1, gsl_vector_get (cal->solVector, 1));
	gsl_matrix_set (cal->calMatrix, 0, 2, gsl_vector_get (cal->solVector, 2));
	gsl_matrix_set (cal->calMatrix, 1, 0, gsl_vector_get (cal->solVector, 3));
	gsl_matrix_set (cal->calMatrix, 1, 1, gsl_vector_get (cal->solVector, 4));
	gsl_matrix_set (cal->calMatrix, 1, 2, gsl_vector_get (cal->solVector, 5));
	
	saveMatrix(cal->calMatrix, "calMatrix.txt");
	
	return cal;
}

int saveMatrix(gsl_matrix * matrix, char * file_name)
{
	FILE *outfile;
	int i,j,m,n;
	float f;
	if ((outfile = fopen(file_name,"w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
  m = matrix -> size1;
  n = matrix -> size2;
  for (i = 0; i < m; i++){
    for (j = 0; j < n; j++){
    	f = gsl_matrix_get(matrix,i,j);
    	fprintf(outfile,"%f ",f);
    }
    fprintf(outfile,"\n");
  }
  fclose(outfile);
  return 0;
}

int saveVector(gsl_vector *v, char * file_name)
{
	FILE *outfile;
	int i,n;
	float f;
	if ((outfile = fopen(file_name,"w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
  n = v->size;
    for (i = 0; i < n; i++){
    	f = gsl_vector_get(v,i);
    	fprintf(outfile,"%f ",f);
    }
    fprintf(outfile,"\n");
   fclose(outfile);
    return 0;
}

void
affine_calibration_destroy (affine_calibration_t *cal)
{
	if (cal == NULL)
        return;
        
    if (cal->calMatrix != NULL)
        free (cal->calMatrix);
    if (cal->worldVector != NULL)
        free (cal->worldVector);
    if (cal->solVector != NULL)
        free (cal->solVector);
    if (cal->AMatrix != NULL)
        free (cal->AMatrix);
    memset (cal, 0, sizeof(*cal));
    free (cal);
}

void
affine_calibration_print (const zarray_t *pairs)
{
	// create and calculate matrix
	
	affine_calibration_t *af = calloc (1, sizeof(*af));
	affine_pair_t *affine_pair = calloc (1, sizeof(*affine_pair));
	
	int i;
	for(i=0;i<zarray_size(pairs);i++)
	{
		zarray_get (pairs, i, affine_pair);
		printf("pixel %d: (%f,%f)\n",i,affine_pair->pixel[0],affine_pair->pixel[1]);
	}
	for (i = 0; i<zarray_size(pairs); i++)
	{
		zarray_get(pairs, i, affine_pair);
		printf("world %d: (%f,%f)\n", i, affine_pair->world[0], affine_pair->world[1]);
	}
	free (affine_pair);
}

// functions to go to pixel->world and world->pixel
void
affine_calibration_p2w (const affine_calibration_t *cal, const double pixels[2], double world[2])
{
	gsl_vector * pixel = gsl_vector_calloc (3);
	gsl_vector * world_vec = gsl_vector_calloc (3);
	
	gsl_vector_set(pixel, 0, pixels[0]);
	gsl_vector_set(pixel, 1, pixels[1]);
	
	gsl_blas_dgemv(CblasNoTrans,1.0,cal->calMatrix,pixel,0.0,world_vec);
	world[0] = gsl_vector_get(world_vec, 0);
	world[1] = gsl_vector_get(world_vec, 1);
	
}
						
void
affine_calibration_w2p (const affine_calibration_t *cal, const double world[2], double pixels[2])
{
	gsl_vector * pixel = gsl_vector_calloc (3);
	gsl_vector * world_vec = gsl_vector_calloc (3);
	
	gsl_vector_set(world_vec, 0, world[0]);
	gsl_vector_set(world_vec, 1, world[1]);
	
	gsl_matrix *calInv = gsl_matrix_calloc (3, 3);
	gsl_matrix_set_identity (calInv);
	double detRot = gsl_matrix_get(cal->calMatrix, 0, 0)*gsl_matrix_get(cal->calMatrix, 1, 1)-gsl_matrix_get(cal->calMatrix, 0, 1)*gsl_matrix_get(cal->calMatrix, 1, 0);
	gsl_matrix_set (calInv, 0, 0, gsl_matrix_get(cal->calMatrix, 1, 1)/detRot);
	gsl_matrix_set (calInv, 0, 1, -1*gsl_matrix_get(cal->calMatrix, 0, 0)/detRot);
	gsl_matrix_set (calInv, 1, 0, -1*gsl_matrix_get(cal->calMatrix, 0, 0)/detRot);
	gsl_matrix_set (calInv, 1, 1, gsl_matrix_get(cal->calMatrix, 0, 0)/detRot);
	gsl_matrix_set (calInv, 0, 2, -1*gsl_matrix_get(cal->calMatrix, 0, 2));
	gsl_matrix_set (calInv, 1, 2, -1*gsl_matrix_get(cal->calMatrix, 1, 2));
	//saveMatrix(calInv, "calMatrixInv.txt");
	gsl_blas_dgemv(CblasNoTrans,1.0,calInv,world_vec,0.0,pixel);
	
	pixels[0] = gsl_vector_get(pixel, 0);
	pixels[1] = gsl_vector_get(pixel, 1);
}
/*					
// useful for Vx
const matd_t *
affine_calibration_p2w_mat (const affine_calibration_t *cal)
{
	
}

const matd_t *
affine_calibration_w2p_mat (const affine_calibration_t *cal)
{
	
}

gsl_matrix_const_view
affine_calibration_p2w_gsl (const affine_calibration_t *cal)
{
	
}

gsl_matrix_const_view
affine_calibration_w2p_gsl (const affine_calibration_t *cal)
{

}
*/
