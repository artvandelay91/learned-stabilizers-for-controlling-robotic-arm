#ifndef __AFFINE_CALIBRATION_H__
#define __AFFINE_CALIBRATION_H__

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include "common/zarray.h"
#include "math/matd.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct affine_pair affine_pair_t;
struct affine_pair {
	double pixel[2];
	double world[2];
};

typedef struct affine_calibration affine_calibration_t;
struct affine_calibration {
	gsl_matrix *AMatrix;
	gsl_vector *worldVector;
	gsl_vector *solVector;
	gsl_matrix *calMatrix;
};

// init/destroy
affine_calibration_t *
affine_calibration_create (const zarray_t *pairs);

void
affine_calibration_destroy (affine_calibration_t *cal);

int saveMatrix(gsl_matrix * matrix, char * file_name);
int saveVector(gsl_vector * vector, char * file_name);

// functions to go to pixel->world and world->pixel
void
affine_calibration_p2w (const affine_calibration_t *cal,
						const double pixels[2], double world[2]);
						
void
affine_calibration_w2p (const affine_calibration_t *cal,
						const double world[2], double pixels[2]);
						
// useful for Vx
const matd_t *
affine_calibration_p2w_mat (const affine_calibration_t *cal);

const matd_t *
affine_calibration_w2p_mat (const affine_calibration_t *cal);

gsl_matrix_const_view
affine_calibration_p2w_gsl (const affine_calibration_t *cal);

gsl_matrix_const_view
affine_calibration_w2p_gsl (const affine_calibration_t *cal);

#ifdef __cplusplus
}
#endif

#endif // __AFFINE_CALIBRATION_H__
